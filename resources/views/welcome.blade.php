@include('layouts.header')

<body class="login-content text-center">


<div class="lc-block toggled text-center" id="l-login">
    <form class="form-horizontal text-center" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}

    <h3 class="text-uppercase">IT<br>-<br> <b class="h1">ISSUES</b> </h3>
    <div class="input-group m-b-20">
        <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
        <div class="fg-line">
            <input  name="login" value="{{ old('login') }}" required autofocus type="text" class="form-control" placeholder="Saisissez votre dentifiant">
        </div>
    </div>

    <div class="input-group m-b-20">
        <span class="input-group-addon"><i class="zmdi zmdi-key"></i></span>
        <div class="fg-line">
            <input name="password" required type="password" class="form-control" placeholder="Saisissez votre mot de passe">
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="checkbox">
        <label>
            <input type="checkbox" value="">
            <i class="input-helper"></i>
            Se souvenir de Moi
        </label>
    </div>

    <button type="submit" class="btn btn-login btn-success btn-float">
        <b><i class="zmdi zmdi-arrow-forward"></i></b>
    </button>

    <ul class="login-navigation">
        <li data-block="" class="bgm-blue">
            Bienvenue à Vous
        </li>
        <li data-block="" class="bgm-green">
           Saisissez vos identifiants
        </li>
    </ul>

    </form>
    <br>
    &copy;MSC  -  <b>TOGO - 2018</b>
</div>

@include('layouts.footer')