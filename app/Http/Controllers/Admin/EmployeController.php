<?php

namespace App\Http\Controllers\Admin;

use App\Employe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class EmployeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $employes = Employe::all();

        return view('admin.employes.index',compact('employes','user'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('admin.employes.create',compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nomEmp'=>'required',
            'prenomEmp'=>'required',
            'agence'=>'required',
            'departement'=>'required'
        ]);

        $e = new Employe();
        $e->nomEmp = strtoupper($request->nomEmp);
        $e->prenomEmp = ucfirst($request->prenomEmp);
        $e->agence_id = $request->agence;
        $e->departement_id = $request->departement;
        $e->save();

        flash('Employe ajouté avec succès');
        return redirect(route('employe.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $employe = Employe::find($id);
        return view('admin.employes.edit',compact('user','employe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'nomEmp'=>'required',
            'prenomEmp'=>'required',
            'agence'=>'required',
            'departement'=>'required'
        ]);

        $e = Employe::find($id);
        $e->nomEmp = strtoupper($request->nomEmp);
        $e->prenomEmp = ucfirst($request->prenomEmp);
        $e->agence_id = $request->agence;
        $e->departement_id = $request->departement;
        $e->save();

        flash('Employe modifié avec succès');
        return redirect(route('employe.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
