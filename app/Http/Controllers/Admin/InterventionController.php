<?php

namespace App\Http\Controllers\Admin;

use App\Employe;
use App\Intervention;
use App\Observation;
use App\Probleme;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class InterventionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();
        $intervention = Intervention::all();
        $interventions = Intervention::with(['employe','probleme','observation','user'])->orderByDesc('fin')->paginate(10);

        foreach ($interventions as $oneIntervention){

            $formatt = explode('-',$oneIntervention->duration1);
            if (count($formatt)>=3 ){
                $formatte = explode(' ',$formatt[2]);
                //dd($formatt);

                $oneIntervention->duration1 = $formatt[0].'-'.(str_pad($formatt[1]-1,2,'0',STR_PAD_LEFT)).'-'.(str_pad($formatte[0]-1,2,'0',STR_PAD_LEFT)).' '.($formatte[1]);
            }
        }

        if ($request->ajax()){
            return DataTables::of($intervention)->make(true);
        }

        return view('admin.interventions.index',compact('interventions','intervention','user'));
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function binder(){
        return DataTables::of(Intervention::query())->make(true);
    }

    public function alertecreate($id){
        $user = Auth::user();
        $employes = Employe::all();
        $problemes = Probleme::all();
        $observations = Observation::all();

        return view('admin.interventions.create', compact('user','problemes','employes','observations'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        $employes = Employe::all();
        $problemes = Probleme::all();
        $observations = Observation::all();
        return view('admin.interventions.create',compact('user','problemes','employes','observations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \DateTime $datedebut
     * @param \DateTime $datefin
     * @param null $param
     * @return string
     */

    public static function dur(\DateTime $datedebut, \DateTime $datefin, $param = null){
        $temps = $datedebut->diff($datefin, true);
        /**
         * valeur possible de $param attendu
         *
         * %Y en année
         * %m en mois
         * %d en jours
         * %H en heures
         * %i en minutes
         * %s en secondes
         *
         */
        if ($param != null){
            return $temps->format($param);
        }else{
            return $temps->format('%m mois %d jrs  %H H %i min %s s');
        }

    }


//    /**
//     * @param string $date
//     * @param string $duration
//     * @return string
//     * @throws \Exception
//     */
//    public function durationSum($date,$duration){
//
//        //la duration est en minute;
//       // $dateInterval = 'PT'.$duration.'M';
//
//        /**
//        si la duration est en date exemple 01:35:00
//         * decommenter ce bloc et commenter celui de dessus
//         */
//        $duration = new DateTime($duration);
//        $duration_year = $duration->format('Y').'Y';
//        $duration_month = 1-$duration->format('m').'M';
//        $duration_day = 1-$duration->format('d').'D';
//        $duration_hour = $duration->format('H').'H';
//        $duration_min = $duration->format('i').'M';
//        $duration_second = $duration->format('s').'S';
//
//        $dateInterval = 'P'.$duration_year.$duration_month.$duration_day.'T'.$duration_hour.$duration_min.$duration_second;
//
//
//        $date = new DateTime($date);
//        $date->add(new DateInterval($dateInterval));
//        return $date->format('Y-m-d H:i:s') . "\n";
//    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'employe'=>'required',
            'probleme'=>'required',
            'description'=>'required',
            'observation'=>'required',
      ]);

        $e = new Intervention();
        $e->user_id = Auth::id();

        if (isset($_POST['intervenir-name'],$_POST['intervenir-surname']) && !empty($_POST['intervenir-name']) && !empty($_POST['intervenir-surname'])){
            $intervenir_name =  $_POST['intervenir-name'];
            $intervenir_surname =  $_POST['intervenir-surname'];

            $employe = Employe::query()->where('nomEmp',$intervenir_name)
                                        ->where('prenomEmp',$intervenir_surname)->firstOrFail();
            $e->employe_id = $employe->id;
        }else{
            $e->employe_id = $request->employe;
        }


        $e->probleme_id = $request->probleme;
        $e->observation_id = $request->observation;
        $e->descriptionPro = ucfirst($request->descriptionPro);
        $e->description = ucfirst($request->description);
        $e->debut = new Carbon($request->input('debut'));
        $e->fin = new  Carbon($request->input('fin'));
        $e->duree = self::dur($e->debut,$e->fin,null);

        //$formatt = explode('-',$request->duration1);
        //$formatte = explode(' ',$formatt[2]);
        //dd($formatte);

       // $e->duration1 = $formatt[0].'-'.(str_pad($formatt[1]+1,2,'0',STR_PAD_LEFT)).'-'.(str_pad($formatte[0]+1,2,'0',STR_PAD_LEFT)).' '.($formatte[1]);

//        $e->fin = self::durationSum($e->debut,$e->duration1);
        $e->save();


        flash('Intervention enrégistée avec succès');
        return redirect(route('intervention.index'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function edit($id)
    {
        $user = Auth::user();
        $intervention = Intervention::find($id);
        $problemes = Probleme::all();
        $employes = Employe::all();
        $observations = Observation::all();
        return view('admin.interventions.edit',compact('user','intervention','problemes','employes','observations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'description'=>'required',
            'observation'=>'required',
        ]);

        $e = Intervention::find($id);
        $e->observation_id = $request->observation;
        $e->descriptionPro = ucfirst($request->descriptionPro);
        $e->description = ucfirst($request->description);

        $e->save();

        flash('Intervention modifiée avec succès');
        return redirect(route('intervention.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
