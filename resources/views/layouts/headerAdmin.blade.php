<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->

<!-- Mirrored from 192.185.228.226/projects/ma/1-5-1/jquery/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 21 Oct 2015 02:43:46 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> IT-ISSUES </title>

    <!-- Vendor CSS -->
    <link href="{{asset('vendors/bower_components/fullcalendar/dist/fullcalendar.min.css')}} " rel="stylesheet">
    <link href="{{asset('vendors/bower_components/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css')}}" rel="stylesheet">
    <link href=" {{asset('toastr/toastr.min.css')}}" rel="stylesheet" />


    <!-- CSS -->
    <link rel="icon" href="{{asset('img/default.jpg')}}">
    <link href="{{asset('css/app.min.1.css')}}" rel="stylesheet">
    <link href="{{asset('css/app.min.2.css')}}" rel="stylesheet">
    <link href="{{asset('//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css')}}" rel="stylesheet">
    <!-- DataTables Select CSS -->
    <link href="{{asset('css/addons/datatables-select.min.css')}}" rel="stylesheet">
    <!-- DataTables Select CSS -->
    <script href="{{asset('css/addons/datatables-select.min.js')}}" rel="stylesheet"></script>
    <script src="{{ asset('vendor/unisharp/laravel-ckeditor/ckeditor.js') }}"></script>
    <script src="{{asset('vendor/unisharp/laravel-ckeditor/adapters/jquery.js')}}"></script>
    <script>
        CKEDITOR.replace( 'summary-ckeditor' );
    </script>

</head>


<body>

<header id="header">
    <ul class="header-inner">
        <li id="menu-trigger" data-trigger="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>

        <li class="logo hidden-xs">
            <a href="{{route('admin.profil')}}"> IT-ISSUES </a>
        </li>

        <li class="pull-right">
            <ul class="top-menu">
                <li id="toggle-width">
                    <div class="toggle-switch">
                        <input id="tw-switch" type="checkbox" hidden="hidden">
                        <label for="tw-switch" class="ts-helper"></label>
                    </div>
                </li>


                <li class="dropdown">
                    <a data-toggle="dropdown" class="tm-settings" href="#"></a>
                    <ul class="dropdown-menu dm-icon pull-right">
                        <li>
                            <a href="{{route('admin.profil')}}"> {{Auth::user()->nom}} {{Auth::user()->prenom}}</a>
                        </li>
                        <li>
                            <form action="{{route('logout')}}" method="post">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-danger">
                                    <i class="zmdi zmdi-sign-in"></i>
                                    Se Déconnecter</button>
                            </form>
                        </li>

                    </ul>
                </li>
            </ul>


</header>

<section id="main">
    <aside id="sidebar">
        <div class="sidebar-inner c-overflow">
            <div class="profile-menu">
                <a href="#">
                    <div class="profile-pic"> </div>

                    <div class="profile-info">
                        {{Auth::user()->nom}} {{Auth::user()->prenom}}
                        <i class="zmdi zmdi-arrow-drop-down"></i>
                    </div>
                </a>

                <ul class="main-menu">
                    <li>
                        <a href="">
                            <form action="{{route('logout')}}" method="post">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-danger">
                                    <i class="zmdi zmdi-time-restore"></i>
                                    Déconnexion
                                </button>
                            </form>
                        </a>
                    </li>

                </ul>
            </div>

            <ul class="main-menu">
                <li class="active">
                    <a href="{{route('admin.profil')}}">
                        <i class="zmdi zmdi-home"></i>
                        Acceuil
                    </a>
                </li>

                @if (Auth::user()->type==0)
                    <li class="sub-menu">
                        <a href=""><i class="zmdi zmdi-notifications-active"></i> Alertes </a>

                        <ul>
                            {{--<li><a href="{{route("alerte.index")}}"> Liste des alertes</a></li>--}}
                            <li><a href="{{route('alerte.create')}}">Emettre une alerte</a></li>
                        </ul>
                    </li>
                @endif

                @if(Auth::user()->type==1)
                    <li class="sub-menu">
                        <a href=""><i class="zmdi zmdi-notifications-active"></i> Alertes </a>

                        <ul>
                            <li><a href="{{route("alerte.index")}}"> Liste des alertes</a></li>
                            <li><a href="{{route('alerte.create')}}">Emettre une alerte</a></li>
                        </ul>
                    </li>

                    <li class="sub-menu">
                        <a href=""><i class="zmdi zmdi-book"></i> Forum </a>

                        <ul>
                            <li><a href="{{route("forum.index")}}"> Liste des types de résolutions</a></li>
                            <li><a href="{{route('forum.create')}}">Ajouter une résolution</a></li>
                        </ul>
                    </li>

                    <li class="sub-menu">
                        <a href=""><i class="zmdi zmdi-account"></i> Employés </a>

                        <ul>
                            <li><a href="{{route("employe.index")}}"> Liste des employés</a></li>
                            <li><a href="{{route('employe.create')}}">Ajouter un employé</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href=""><i class="zmdi zmdi-folder-person"></i>Utilisateurs</a>

                        <ul>
                            <li><a href="{{route("intervenant.index")}}"> Liste des utilisateurs</a></li>
                            <li><a href="{{route('intervenant.create')}}">Ajouter un utilisateur</a></li>
                        </ul>
                    </li>

                    <li class="sub-menu">
                        <a href=""><i class="zmdi zmdi-refresh-sync-problem"></i> Activités & problèmes </a>
                        <ul>
                            <li><a href="{{route("probleme.index")}}">Liste des activités </a></li>
                            <li><a href="{{route("probleme.create")}}">Ajouter une activité ou un problème </a></li>
                        </ul>
                    </li>

                    <li class="sub-menu">
                        <a href=""><i class="zmdi zmdi-car-taxi"></i> Interventions </a>

                        <ul>
                            <li><a href="{{route("intervention.index")}}">Liste des Interventions</a></li>
                            <li><a href="{{route("intervention.create")}}">Ajouter une intervention</a></li>
                        </ul>
                    </li>

                    <li class="sub-menu">
                        <a href=""><i class="zmdi zmdi-chart"></i> Etats </a>
                        <ul>
                            {{--<li><a href="{{route('admin.total_etats.index')}}"> Groupe de totaux</a></li>--}}
                            <li><a href="{{route('admin.prnr.index')}}"> Problèmes résolus & non résolus</a></li>
                            <li><a href="{{route('admin.intervenants_par_interventions.index')}}"> Interventions pour un intervenant</a></li>
                            {{--<li><a href="{{route('admin.total_intervention_intervenant.index')}}"> Interventions par intervenant</a></li>--}}
                            <li><a href="{{route('admin.interventions_par_employe.index')}}"> Intervention par employé</a></li>
                            <li><a href="{{route('admin.intervention_par_departement.index')}}"> Interventions par département</a></li>
                            <li><a href="{{route('admin.interventions_par_problemes.index')}}"> Intervention par problème</a></li>
                            {{--<li><a href="{{route('admin.intervention_par_intervenant_pour_departement.index')}}"> Intervention par intervenant pour chaque département</a></li>--}}
                            <li><a href="{{route('admin.intervention_par_probleme_departement.index')}}">Interventions par problèmes pour chaque département</a></li>
                        </ul>
                    </li>

                @endif


            </ul>
        </div>
    </aside>

    <section id="content">
        <div class="container">
