@include('layouts.headerAdmin')

<div class="row">
    <div class="col-lg-12">
        <h3 class="text-center">Modification du problème</h3>
            <form class="col-lg-8 col-lg-offset-2 text-center" action="{{route('probleme.update',$probleme->id)}}" method="post">
                {{csrf_field()}}
                {{method_field('PUT')}}
                <div class="form-group">
                    <label>Libellé</label>
                    <input class="form-control" type="text" name="libelle" required value="{{$probleme->libelle}}">
                    @if ($errors->has('libelle'))
                        <p class="text-danger">{{ $errors->first('libelle') }}</p>
                    @endif
                </div>

                {{--<div class="form-group">--}}
                    {{--<label>Description</label>--}}
                    {{--<textarea class="form-control" required name="description" cols="30" rows="10">{{$probleme->description}}</textarea>--}}
                    {{--@if ($errors->has('description'))--}}
                        {{--<p class="text-danger">{{ $errors->first('description') }}</p>--}}
                    {{--@endif--}}
                {{--</div>--}}

                <a type="button" href="{{route('probleme.index')}}" class="btn btn-warning">Annuler</a>

                <button type="submit" class="btn btn-primary">Valider</button>
            </form>
        </div>
    </div>

@include('layouts.footerAdmin')