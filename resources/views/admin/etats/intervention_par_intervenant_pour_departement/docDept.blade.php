<html>
<head>
    <meta charset="utf-8">
    <link href="{{asset('css/app.min.2.css')}} " rel="stylesheet">
    <link href="{{asset('template/css/bootstrap.css')}} " rel="stylesheet">
    <link href="{{asset('ressources/views/vendor/mail/html/themes/default.css')}} " rel="stylesheet">
    <link href="{{asset('_tables.scss')}} " rel="stylesheet">
</head>
<body>
<div>
    <div class="invoice center-block" id="printarea">
        <div class="row text-center">
            <div class="col-lg-2 invoice text-center" style="text-align-all: center">
                <img class="" style="height: 150px; width: 190px" src="{{asset('MSC_NOIR.PNG')}}">
                {{--<br><h1>MEDITERRANEAN SHIPPING COMPANY</h1>--}}
            </div>
            <div class="col-lg-8 invoice text-center" style="text-align-all: center">
                <h1>MEDITERRANEAN SHIPPING COMPANY</h1>
            </div>
        </div>
        <hr style="font-weight: bold">
        <div class="row">
            <h2 style="text-align: center;">RAPPORT DU : {{$date_debut}} AU {{$date_fin}}</h2>
            <h3 style="text-align: center;">Nombre d'interventions par intervenant pour chaque département</h3>
            <div class="col-sm-12">
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th>Nom & Prénoms</th>
                        <th>Libellé du département</th>
                        <th>Total</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($data as $r)
                        <tr>
                            <td >{{$r->nom." ".$r->prenom}}</td>
                            <td >{{$r->libelleDept}}</td>
                            <td >{{$r->inter}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel panel-footer">
            <h5>Edité le <?= date('d/m/Y à H:i:s')?>, par {{Auth::user()->nom}} {{Auth::user()->prenom}} </h5>
        </div>
        <button type="button" value="Print" id="btnPrint" onclick="print()" class="btn btn-primary hidden-print">Télécharger</button>
    </div>
</div>
<script type="text/javascript">
    // function print() {
      //  var prtContent = document.getElementById("printarea");
        $('#btnPrint').live("click", function () {
            var divContents = $('#printarea').html();
            var WinPrint = window.open('','','height = 400, width = 800');
            // WinPrint.document.write("<link rel='stylesheet' href='style.css' type='text/css' media='print' />");
            WinPrint.document.write('<html><head><title>Contenu DIV</title>');
            WinPrint.document.write(divContents);
            WinPrint.document.write('</body></html>');
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
        });

        // WinPrint.close();
   // }
</script>
</body>
</html>