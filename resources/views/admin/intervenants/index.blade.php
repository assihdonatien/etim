@include('layouts.headerAdmin')

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <h2>Liste des utilisateurs <a href="{{route('intervenant.create')}}" class="btn btn-primary pull-right">
                        <i class="zmdi zmdi-plus"></i>
                        Nouvel utilisateur </a>
                </h2>
                <table class="table table-bordered table-hover tablesorter">
                    <thead>
                    <tr>
                        <th>Noms <i class="fa fa-sort"></i></th>
                        <th>Prénoms <i class="fa fa-sort"></i></th>
                        <th>Login <i class="fa fa-sort"></i></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($intervenants->sortBy('nom') as $i)
                        <tr>
                            <td>{{$i->nom}}</td>
                            <td>{{$i->prenom}}</td>
                            <td>{{$i->email}}</td>
                            <td>{{$i->login}}</td>
                            {{--<td>--}}
                               {{--<a href="<{{route('intervenant.edit',$i->id)}}>">--}}
                                    {{--<button class="btn btn-warning" > <i class="zmdi zmdi-edit"></i></button>--}}
                                {{--</a>--}}
                            {{--</td>--}}
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@include('layouts.footerAdmin')
