<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->

<!-- Mirrored from 192.185.228.226/projects/ma/1-5-1/jquery/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 21 Oct 2015 02:43:46 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> IT-ISSUES </title>

    <!-- Vendor CSS -->
    <link href="{{asset('vendors/bower_components/fullcalendar/dist/fullcalendar.min.css')}} " rel="stylesheet">
    <link href="{{asset('vendors/bower_components/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css')}}" rel="stylesheet">



    <!-- CSS -->
    <link rel="icon" href="{{asset('img/default.jpg')}}">
    <link href="{{asset('css/app.min.1.css')}}" rel="stylesheet">
    <link href="{{asset('css/app.min.2.css')}}" rel="stylesheet">

</head>


<body>

<header id="header">
    <ul class="header-inner">
        <li id="menu-trigger" data-trigger="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>

        <li class="logo hidden-xs">
            <a href="{{route('admin.profil')}}"> IT-ISSUES </a>
        </li>

        <li class="pull-right">
            <ul class="top-menu">
                <li id="toggle-width">
                    <div class="toggle-switch">
                        <input id="tw-switch" type="checkbox" hidden="hidden">
                        <label for="tw-switch" class="ts-helper"></label>
                    </div>
                </li>
                <li id="top-search">
                    <a class="tm-search" href="#"></a>
                </li>

                <li class="dropdown">
                    <a data-toggle="dropdown" class="tm-settings" href="#"></a>
                    <ul class="dropdown-menu dm-icon pull-right">
                        <li>
                            <a href="{{route('admin.profil')}}"> {{Auth::user()->nom}} {{Auth::user()->prenom}}</a>
                        </li>
                        <li>
                            <form action="{{route('logout')}}" method="post">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-danger">
                                    <i class="zmdi zmdi-sign-in"></i>
                                    Se Déconnecter</button>
                            </form>
                        </li>

                    </ul>
                </li>
            </ul>

            <!-- Top Search Content -->
            <div id="top-search-wrap">
                <input type="text">
                <i id="top-search-close">&times;</i>
            </div>
</header>

<section id="main">
    <aside id="sidebar">
        <div class="sidebar-inner c-overflow">
            <div class="profile-menu">
                <a href="#">
                    <div class="profile-pic"> </div>

                    <div class="profile-info">
                        {{Auth::user()->nom}} {{Auth::user()->prenom}}
                        <i class="zmdi zmdi-arrow-drop-down"></i>
                    </div>
                </a>

                <ul class="main-menu">
                    <li>
                        <a href="">
                            <form action="{{route('logout')}}" method="post">
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-danger">
                                    <i class="zmdi zmdi-time-restore"></i>
                                    Déconnexion
                                </button>
                            </form>
                        </a>
                    </li>

                </ul>
            </div>

            <ul class="main-menu">
                <li class="active">
                    <a href="{{route('home')}}">
                        <i class="zmdi zmdi-home"></i>
                        Acceuil
                    </a>
                </li>

                <li class="sub-menu">
                    <a href=""><i class="zmdi zmdi-notifications-active"></i> Alertes </a>

                    <ul>
                        <li><a href="{{route("alerte.index")}}"> Liste des alertes</a></li>
                        <li><a href="{{route('alerte.create')}}">Emettre une alerte</a></li>
                    </ul>
                </li>

            </ul>
        </div>
    </aside>

    <section id="content">
        <div class="container">
