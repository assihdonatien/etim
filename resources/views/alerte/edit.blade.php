@include('layouts.headerAdmin')


<div class="row">
    <div class="col-lg-12">
        <h3 class="text-center">Modification intervention</h3>

        <form class="col-lg-8 col-lg-offset-2 text-center" action="{{route('alerte.update', $alerte->idAlerte)}}" method="post">
            {{csrf_field()}}
            {{method_field('PUT')}}

            <div class="form-group">
                <label>Titre de l'alerte</label>
                <input type="text" name="title" value="{{$alerte->titre_alerte}}">
                @if ($errors->has('titre_alerte'))
                    <p class="text-danger">{{ $errors->first('titre_alerte') }}</p>
                @endif
            </div>

            <div class="form-group">
                <label>Description de l'alerte</label>
                <textarea name="description" id="" cols="30" rows="5" class="form-control" >{{$alerte->description_alerte}}</textarea>
                @if ($errors->has('description_alerte'))
                <p class="text-danger">{{ $errors->first('description_alerte') }}</p>
                @endif
            </div>

            {{--<div class="form-group">--}}
                {{--<label for="">Date de début intervention</label>--}}
                {{--<input type="datetime-local" name="debut" class="form-control">--}}
                {{--@if ($errors->has('debut'))--}}
                    {{--<p class="text-danger">{{ $errors->first('debut') }}</p>--}}
                {{--@endif--}}
            {{--</div>--}}

            <a type="button" href="{{route('alerte.index')}}" class="btn btn-warning">Annuler</a>

            <button type="submit" class="btn btn-primary">Valider</button>

        </form>
    </div>
</div>

@include('layouts.footerAdmin')
