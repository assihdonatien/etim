<?php
/**
 * Created by IntelliJ IDEA.
 * User: Simone Sika
 * Date: 20/03/2019
 * Time: 17:27
 */

namespace App\Http\Controllers\Admin;


use App\Forum;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ForumController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $forum = Forum::all();
        return view('forum.index',compact('forum','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('forum.create',compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('document');

        $this->validate($request,[
            'probleme'=>'required',
            'contenu'=>'required',
        ]);

        $user = Auth::user();

        $forum = new Forum();
        $forum->id_probleme = $request->probleme;
        $forum->content = $request->contenu;
        $destinationPath = "uploads";
        $filename = $file[0]->getClientOriginalName();

        if ($file[0]->move($destinationPath,$filename)){
            $forum->document = $filename;
            if ($forum->save()){
                flash('Forum ajouté avec succès');

                return redirect(route('forum.index'));
            }
        }
        return redirect(route('forum.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $alerte = Alerte::query()->where('idAlerte',$id)->firstOrFail();
        return view('alerte.edit',compact('user','alerte'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'libelle'=>'required',
        ]);

        $e = Alerte::find($id);
        $e->libelle= ucfirst($request->libelle);
//        $e->description = $request->description;
        $e->save();

        flash('Problème mis à jour avec succès');
        return redirect(route('probleme.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}