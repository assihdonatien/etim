<?php
/**
 * @author Donatien ASSIH
 */

namespace App\Http\Controllers\Admin;

use App\Alerte;
use App\Mail\Email;
use App\Observation;
use App\Probleme;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Psy\Util\Json;

class AlerteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



//    public function index()
//    {
//        $user = Auth::user();
//        $alertes = Alerte::all();
//        return view('alerte.index', compact('alertes', 'user'));
//    }



    public function alerting()
    {
        $not_threat = Alerte::where('etat_alerte', false)->get();
        return Json::encode($not_threat);
    }

    public function index()
    {
        $user = Auth::user();
        if (isset($_GET['id'])){
            $id = $_GET['id'];
            $alerte = Alerte::query()->where('id',$id)->firstOrFail();
            $utilisateurs = User::query()->where('id',$alerte->employe_id_alerte)->firstOrFail();}
        //$alertes = Alerte::all();
        $alertes = Alerte::where('alertes.etat_alerte',false)->get();
        return view('alerte.index',compact('alertes','user','utilisateurs','alerte'));
    }

//    public function alerting(){
//
////        $not_threat = DB::table('alertes')
////            ->select(DB::raw('users.*,alertes.*'))
////            ->join('users','alertes.employe_id_alerte','=','users.id')
////            ->where('etat_alerte',false)
////            ->get();
//
//        $not_threat = DB::select('select *'. 'from alertes, users' .'where alertes.employe_id_alerte = users.id' . 'and etat_alerte = false');
//
//        return Json::encode($not_threat);
//    }


    public function intervenir()
    {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $alerte = Alerte::query()->where('id', $id)->firstOrFail();
            $utilisateurs = User::query()->where('id', $alerte->employe_id_alerte)->firstOrFail();

            $problemes = Probleme::all();
            $observations = Observation::all();
            $description = $alerte->description_alerte;
            $debut = $alerte->date_alerte;
            $alerte->etat_alerte = true;
            /*
             * TODO
             *
            */
            $alerte->save();
            return view('alerte.intervenir', compact('user', 'problemes', 'utilisateurs', 'observations', 'description','debut'));
        } else {
            dd('Can\'t intervenir');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('alerte.create', compact('user'));
    }

    /*public function sendemail(Alerte $alerte)
    {
        $emails = User::query('email')->where('type', 1)->get();
        $tos[] = null;
        foreach ($emails as $oneemail){
            $tos[] = $oneemail->email;
        }

        $sender = User::where('id', $alerte->employe_id_alerte)->firstOrFail();
        $title = $sender->nom . " " . $sender->prenom . ": ITSSUES";
        $content = "$alerte->titre_alerte \n$alerte->description_alerte";
        $user_name = $sender->nom . " " . $sender->prenom;

        try {
            $data = [
                'email' => $sender->email,
                'name' => $user_name,
                'subject' => $title,
                'content' => $content,
            ];

            Mail::queue('alerte.email', $data, function ($tos,$message) use ($data) {
                $subject=$data['subject'];
                $message->from($data['email'],$data['name']);
                $message->to($tos);
                $message->subject($subject);
            });
        } catch (\Exception $exception) {
            dd($exception->getMessage());
        }
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'titre_alerte' => 'required',
            'description_alerte' => 'required',
        ]);

        $user = Auth::user();

        $alerte = new Alerte();
        $alerte->employe_id_alerte = $user->id;
//         $alerte->employe_id_alerte = Auth::user();
        $alerte->titre_alerte = strtoupper($request->titre_alerte);
//        $alerte->titre_alerte = $request->probleme;
        $alerte->description_alerte = ucfirst($request->description_alerte);
        $alerte->date_alerte = Carbon::now();
        $alerte->etat_alerte = false;

        if ($alerte->save()) {
            $emails = User::query('email')->where('type', 1)->get();
            foreach ($emails as $oneemail){
                $tos[] = $oneemail['email'];
            }

            Mail::to($tos)->send(new Email($alerte));

            flash('Alerte ajouté avec succès');

            return redirect(route('alerte.index'));
        }

        return redirect(route('alerte.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $alerte = Alerte::query()->where('id', $id)->firstOrFail();
        return view('alerte.edit', compact('user', 'alerte'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'libelle' => 'required',
        ]);

        $e = Alerte::find($id);
        $e->libelle = ucfirst($request->libelle);
//        $e->description = $request->description;
        $e->save();

        flash('Problème mis à jour avec succès');
        return redirect(route('probleme.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
