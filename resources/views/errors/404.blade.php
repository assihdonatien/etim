<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->

<!-- Mirrored from 192.185.228.226/projects/ma/1-5-1/jquery/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 21 Oct 2015 02:51:58 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <!-- Vendor CSS -->
    <link href="{{asset('vendors/bower_components/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css')}}" rel="stylesheet">

    <!-- CSS -->
    <link rel="icon" href="{{asset('img/default.jpg')}}">
    <link href="{{asset('css/app.min.1.css')}}" rel="stylesheet">
    <link href="{{asset('css/app.min.2.css')}}" rel="stylesheet">
</head>

<body class="four-zero-content">
<div class="four-zero">
    <h3>404 Error! </h3>
    <small> Une erreur est survenue!!! Contacter l'administrateur.</small><br><br>

    <footer>
        <a href="{{route('login')}}"><i class="zmdi zmdi-arrow-back"></i></a>
        <a href="{{route('admin.profil')}}"><i class="zmdi zmdi-home"></i></a>
    </footer>
</div>

</body>

</html>