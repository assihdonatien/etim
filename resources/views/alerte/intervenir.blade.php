@include('layouts.headerAdmin')

<div class="row">
    <div class="col-lg-12">
        <h3 class="text-center">Nouvelle intervention</h3>

        <form class="col-lg-8 col-lg-offset-2 text-center" action="{{route('intervention.store')}}" method="post">
            {{csrf_field()}}

            <div class="form-group">
                <label>Utilisateur</label>
                <select name="employe" id="" class="form-control">
                    <option value="<?=$utilisateurs->id?>"><?=$utilisateurs->nom." ".$utilisateurs->prenom?></option>
                </select>
                @if ($errors->has('employe'))
                <p class="text-danger">{{ $errors->first('employe') }}</p>
                @endif
            </div>
            <div class="form-group">
                <label>Problème</label>
                <select name="probleme" id="" class="form-control">
                    <option value="">Sélectionnez un problème</option>
                    @foreach($problemes->sortBy('libelle') as $r)
                    <option value="{{$r->id}}">{{$r->libelle}}</option>
                    @endforeach
                </select>
                @if ($errors->has('probleme'))
                <p class="text-danger">{{ $errors->first('probleme') }}</p>
                @endif
            </div>

            <div class="form-group">
                <label>Description du Problème</label>
                <textarea name="descriptionPro" id="" cols="30" rows="5" class="form-control">{{$description}}</textarea>
                @if ($errors->has('descriptionPro'))
                <p class="text-danger">{{ $errors->first('descriptionPro') }}</p>
                @endif
            </div>

            <div class="form-group">
                <label>Description de la résolution</label>
                <textarea name="description" id="" cols="30" rows="5" class="form-control"></textarea>
                @if ($errors->has('description'))
                    <p class="text-danger">{{ $errors->first('description') }}</p>
                @endif
            </div>

            <div class="form-group">
                <label>Observations</label>
                <select name="observation" id="" class="form-control">
                    <option value="">Sélectionnez une observation</option>
                    @foreach($observations as $r)
                    <option value="{{$r->id}}">{{$r->libelleObservation}}</option>
                    @endforeach
                </select>
                @if ($errors->has('observation'))
                <p class="text-danger">{{ $errors->first('observation') }}</p>
                @endif
            </div>

            {{--<div class="form-group">--}}
                {{--<label for="">Date de début intervention</label>--}}
                {{--<input type="datetime-local" name="debut" class="form-control">--}}
                {{--@if ($errors->has('debut'))--}}
                {{--<p class="text-danger">{{ $errors->first('debut') }}</p>--}}
                {{--@endif--}}
            {{--</div>--}}

            <div class="form-group">
                <label for="">Date de début intervention</label>
                <input type="datetime-local" name="debut" class="form-control" value="{{$debut}}">
                @if ($errors->has('debut'))
                    <p class="text-danger">{{ $errors->first('debut') }}</p>
                @endif
            </div>



            <div class="form-group">
                <label for="">Date de fin intervention</label>
                <input type="datetime-local" name="fin" class="form-control">
                @if ($errors->has('fin'))
                    <p class="text-danger">{{ $errors->first('fin') }}</p>
                @endif
            </div>


            <div class="form-group">
                <input type="hidden" name="intervenir-name" class="form-control" value="<?=$utilisateurs->nom?>">
                <input type="hidden" name="intervenir-surname" class="form-control" value="<?=$utilisateurs->prenom?>">
            </div>

            {{--<div class="form-group">--}}
                {{--<label for="">Durée de l'intervention</label>--}}
                {{--<input type="text" name="duration1" class="form-control">--}}
                {{--@if ($errors->has('duration1'))--}}
                    {{--<p class="text-danger">{{ $errors->first('duration1') }}</p>--}}
                {{--@endif--}}
            {{--</div>--}}

            <button type="submit" class="btn btn-primary">Valider</button>
        </form>
    </div>
</div>

@include('layouts.footerAdmin')