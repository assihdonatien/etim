@include('layouts.headerAdmin')

    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <h2>Liste des Problèmes <a href="{{route('probleme.create')}}" class="btn btn-primary pull-right"> Nouveau problème</a>  </h2>
                <table class="table table-bordered table-hover tablesorter">
                    <thead>
                    <tr>
                        <th>Libellé <i class="fa fa-sort"></i></th>
                        <th>Description <i class="fa fa-sort"></i></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($problemes->sortBy('libelle') as $r)
                        <tr>
                            <td>{{$r->libelle}}</td>
                            {{--<td>{{$r->descriptionPro}}</td>--}}
                            <td>
                                <a href="{{route('probleme.edit',$r->id)}}">
                                    <button class="btn btn-warning" > <i class="zmdi zmdi-edit"></i></button>
                                </a>
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@include('layouts.footerAdmin')
