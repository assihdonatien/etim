<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alerte extends Model
{
    protected $fillable = [
        'id','employe_id_alerte','titre_alerte','description_alerte','etat_alerte','users_id_alerte','date_alerte'
    ];
}
