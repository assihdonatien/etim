<?php

namespace App\Http\Controllers\Admin;

use App\Fonction;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class IntervenantController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $intervenants = User::all();
        return view('admin.intervenants.index',compact('intervenants','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        return view('admin.intervenants.create',compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nom'=>'required',
            'prenom'=>'required',
            'type'=>'required',
            'login'=>'required|unique:users,login',
            'password'=>'required|string|min:6|confirmed',
        ]);

        $e = new User();
        $e->nom = strtoupper($request->nom);
        $e->fonction_id = $request->fonction;
        $e->prenom =  ucfirst($request->prenom);
        $e->email =  $request->email;
        $e->login = $request->login;
        $e->password = bcrypt($request->password);
       // $e->password = $request->password;
        $e->type = $request->type;
        $e->first_connection = 0;
        $e->save();

        flash('Intervenant ajouté avec succès');
        return redirect(route('intervenant.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Auth::user();
        $intervenant = User::find($id);
       return view('admin.intervenants.edit',compact('user','intervenant'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nom'=>'required',
            'prenom'=>'required',
            'type'=>'required',
        ]);

        $e = Fonction::find($id);
        $e->nom = strtoupper($request->nom);
        $e->prenom = ucfirst($request->prenom);
        $e->type = $request->type;
        $e->fonction_id = $request->fonction;
        $e->password = bcrypt($request->password);
        $e->save();

        flash('Intervenant modifié avec succès');
        return redirect(route('intervenant.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
