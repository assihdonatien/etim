{{--@include('layouts.headerAdmin')--}}

{{--<div class="row">--}}
    {{--<div class="col-lg-3">--}}
        {{--<img class="img-responsive " style="height: 250px; width: 250px" src="{{asset('MSC_NOIR.PNG')}}">--}}
    {{--</div>--}}
    {{--<div class="col-lg-6">--}}
        {{--<br><br><br><br><br>--}}
        {{--<h3>MEDITERRANEAN SHIPPING COMPANY</h3>--}}
    {{--</div>--}}
{{--</div>--}}
{{--<div class="row">--}}
    {{--<div class="col-lg-12">--}}
        {{--<h3 class="text-center">Totaux des états</h3>--}}
        {{--<div class="table-responsive">--}}
            {{--<table class="table table-bordered table-hover tablesorter">--}}
                {{--<thead>--}}
                {{--<tr>--}}
                    {{--<th>Nombre de problèmes<i class="fa fa-sort"></i></th>--}}
                    {{--<th>Nombre d'intervenants <i class="fa fa-sort"></i></th>--}}
                    {{--<th>Nombre d'interventions <i class="fa fa-sort"></i></th>--}}
                    {{--<th>Nombre d'employés ayant eu une intervention <i class="fa fa-sort"></i></th>--}}
                {{--</tr>--}}
                {{--</thead>--}}
                {{--<tbody>--}}
                {{--@foreach($employe as $r)--}}
                    {{--<tr>--}}
                        {{--<td>{{$r->intervenant->nom." ".$r->intervenant->prenom}}</td>--}}
                        {{--<td>{{$r->probleme->libelle}}</td>--}}
                        {{--<td>{{$r->empl}}</td>--}}
                    {{--</tr>--}}
                {{--@endforeach--}}
                {{--</tbody>--}}
            {{--</table>--}}
            {{--<a href="" class="btn btn-primary"> <i class="fa fa-print">Télécharger</i></a>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

{{--@include('layouts.footerAdmin')--}}


<html>
<head>
    <meta charset="utf-8">
</head>
<body>
<div>

    <h3 >Nombre d'interventions par intervenant</h3>
    <div>
        <table style="border:solid black 2px">
            <thead>
            <tr>
                <th>Noms & Prénoms<i class="fa fa-sort"></i></th>
                <th>Problèmes <i class="fa fa-sort"></i></th>
                <th>Total par problèmes <i class="fa fa-sort"></i></th>
            </tr>
            </thead>
            <tbody>
            @foreach($employe as $r)
                <tr>
                    <td style="border:solid black 2px">{{$r->empl}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
</body>
</html>