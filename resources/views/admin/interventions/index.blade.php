@include('layouts.headerAdmin')

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive data-table_filter">
            <h2>Liste des Interventions <a href="{{route('intervention.create')}}" class="btn btn-primary pull-right">
                    <i class="zmdi zmdi-plus"></i>
                    Nouvelle intervention</a>
            </h2>
            <table id="table" class="table table-responsive table-striped table-wrapper-scroll-y table-bordered table-hover datatable" width="100%">
                <thead>
                <tr role="row">
                    <th class="th-sm sorting"><b>Intervenants</b></th>
                    <th class="th-sm sorting"><b>Employés</b> </th>
                    <th class="th-sm sorting"><b>Problèmes ou Activités</b> </th>
                    <th class="th-sm sorting"><b>Description du problème </b></th>
                    <th class="th-sm sorting"><b>Description de la résolution</b></th>
                    <th class="th-sm sorting"><b>Observations</b> </th>
                    <th class="th-sm sorting"><b>Début</b> </th>
                    <th class="th-sm sorting"><b>Fin</b> </th>
                    <th class="th-sm sorting"><b>Durée</b> </th>
                    <th class="th-sm sorting"><b>Duration</b> </th>
                </tr>
                </thead>
              <tbody>
                @foreach($interventions as $e)
                    <tr>
                        <td>{{$e->user->nom." ".$e->user->prenom}}</td>
                        <td>{{$e->employe->nomEmp." ".$e->employe->prenomEmp}}</td>
                        <td>{{$e->probleme->libelle}}</td>
                        <td>{{$e->descriptionPro}}</td>
                        <td>{{$e->description}}</td>
                        <td>{{$e->observation->libelleObservation}}</td>
                        <td><b>{{\Carbon\Carbon::parse($e->debut)->format('d/m/Y H:i:s')}}</b></td>
                        <td><b>{{\Carbon\Carbon::parse($e->fin)->format('d/m/Y H:i:s')}}</b></td>
                        <td><b>{{$e->duree}}</b></td>
                        <td><b>{{$e->duration1}}</b></td>
                        <td>
                            <a href="{{route('intervention.edit',$e->id)}}">
                                <button class="btn btn-warning" > <i class="zmdi zmdi-edit"></i></button>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th class="th-sm">Intervenants</th>
                    <th class="th-sm">Employés </th>
                    <th class="th-sm">Problèmes </th>
                    <th class="th-sm">Description du problème</th>
                    <th class="th-sm">Description de la résolution</th>
                    <th class="th-sm">Observations </th>
                    <th class="th-sm">Début </th>
                    <th class="th-sm">Fin </th>
                    <th class="th-sm">duree </th>
                    <th class="th-sm">duration </th>
                </tr>
                </tfoot>
            </table>
          <center> <?= $interventions->render() ?></center>
        </div>
    </div>
</div>

@include('layouts.footerAdmin')

{{--@section('script')--}}
    {{--<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>--}}
    {{--<script type="text/javascript">--}}
        {{--$(document).ready(function() {--}}
            {{--$('#table').DataTable({--}}
                {{--processing: true,--}}
                {{--serverSide: true,--}}
                {{--ajax: '{{ route('binder') }}',--}}
                {{--columns: [--}}
                    {{--{ data: 'nomEmp', name: 'nomEmp' },--}}
                    {{--{ data: 'prenomEmp', name: 'prenomEmp' },--}}
                    {{--{ data: 'prenom', name: 'prenom' },--}}
                    {{--{ data: 'nom', name: 'nom' }--}}
                {{--]--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
{{--@endsection--}}