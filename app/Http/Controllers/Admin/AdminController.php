<?php

namespace App\Http\Controllers\Admin;

use App\Intervention;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $user = Auth::user();
        return view('admin.profil',compact('user'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
//    public function index1()
//    {
//        $r = Intervention::count()->where('id',0);
//
//        return view('admin.profil',compact('r'));
//
//    }

}
