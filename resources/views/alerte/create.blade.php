@include('layouts.headerAdmin')

    <div class="row">
        <div class="col-lg-12">
            <h3 class="text-center">Emettre une alerte</h3>

            <form class="col-lg-8 col-lg-offset-2 text-center" action="{{route('alerte.store')}}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <label>Titre</label>
                    <input class="form-control" type="text" name="titre_alerte" required value="{{old('titre_alerte')}}">
                    @if ($errors->has('titre_alerte'))
                        <p class="text-danger">{{ $errors->first('titre_alerte') }}</p>
                    @endif
                </div>

                {{--<div class="form-group">--}}
                    {{--<label>Titre</label>--}}
                    {{--<select name="probleme" id="" class="form-control">--}}
                        {{--<option value="">Faites une sélection</option>--}}
                        {{--@foreach(\App\Probleme::all()->sortBy('libelle') as $r)--}}
                            {{--<option value="{{$r->id}}">{{$r->libelle}}</option>--}}
                        {{--@endforeach--}}
                    {{--</select>--}}
                    {{--@if ($errors->has('probleme'))--}}
                        {{--<p class="text-danger">{{ $errors->first('probleme') }}</p>--}}
                    {{--@endif--}}
                {{--</div>--}}


                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" name="description_alerte" id="" cols="30" rows="5">{{old('description_alerte')}}</textarea>
                    @if ($errors->has('description_alerte'))
                        <p class="text-danger">{{ $errors->first('description_alerte') }}</p>
                    @endif
                </div>

                <button type="submit" class="btn btn-primary">Valider</button>
            </form>
        </div>
    </div>
@include('layouts.footerAdmin')
