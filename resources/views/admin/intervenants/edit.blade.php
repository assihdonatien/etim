@include('layouts.headerAdmin')

<div class="row">
    <div class="col-lg-12">
        <h3 class="text-center">Modification d'un intervenant</h3>

        <form class="col-lg-8 col-lg-offset-2 text-center" action="{{route('intervenant.update',$user->id)}}" method="post">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group">
                <label>Nom</label>
                <input class="form-control" type="text" name="nom" required value="{{$user->nom}}">
                @if ($errors->has('nom'))
                    <p class="text-danger">{{ $errors->first('nom') }}</p>
                @endif
            </div>

            <div class="form-group">
                <label>Prénom</label>
                <input class="form-control" type="text" name="prenom" required value="{{$user->prenom}}">
                @if ($errors->has('prenom'))
                    <p class="text-danger">{{ $errors->first('prenom') }}</p>
                @endif
            </div>

            <div class="form-group">
                <label>Fonction</label>
                <select name="fonction" id="" class="form-control">
                    <option value="">Sélectionnez une fonction</option>
                    @foreach(\App\Fonction::all() as $a)
                        <option value="{{$a->id}}">{{$a->libelleFonction}}</option>
                    @endforeach
                </select>
                @if ($errors->has('fonction'))
                    <p class="text-danger">{{ $errors->first('fonction') }}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="">Type</label>
                <select name="type" id="" class="form-control">
                    <option value="">Sélectionnez un type</option>
                    <option value="0">Simple Intervenant</option>
                    <option value="1">Administrateur</option>
                </select>
                @if ($errors->has('type'))
                    <p class="text-danger">{{ $errors->first('type') }}</p>
                @endif
            </div>

            <div class="form-group">
                <label>Mot de passe</label>
                <input class="form-control" type="password" name="password" required>
                @if ($errors->has('password'))
                    <p class="text-danger">{{ $errors->first('password') }}</p>
                @endif
            </div>

            <button type="submit" class="btn btn-primary">Valider</button>
            <a type="button" href="{{route('intervenant.index')}}" class="btn btn-warning">Annuler</a>
        </form>

    </div>
</div>


@include('layouts.footerAdmin')
