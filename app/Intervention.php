<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Intervention extends Model
{

    protected $fillable = [
        'description','user_id','probleme_id','employe_id','debut','fin','duration1'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function employe()
    {
        return $this->belongsTo('App\Employe','employe_id');
    }

    public function probleme()
    {
        return $this->belongsTo('App\Probleme','probleme_id');
    }

    public function observation()
    {
        return $this->belongsTo('App\Observation','observation_id');
    }

}
