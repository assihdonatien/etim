<?php
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

if (!function_exists('set_active_route')) {
    function set_active_route($route)
    {
        return Route::is($route) ? 'active' : '';
    }
}

if (!function_exists('flash'))
{
    function flash($message,$type='success')
    {
        session()->flash('notification.message',$message);
        session()->flash('notification.type',$type);
    }
}


if (!function_exists('set_image')) {
    function set_image($image)
    {
        if ($image==''){
            $r='/img/user.png';
        }else $r=$image;

        return $r;
    }
}
if (!function_exists('set_sexe')) {
    function set_sexe($sexe)
    {
        if ($sexe =='M'){
            $r='Masculin';
        }else $r= 'Féminin';

        return $r;
    }
}

if (!function_exists('set_genre')) {
    function set_genre($sexe)
    {
        if ($sexe =='M'){
            $r='M';
        }else $r= 'Mme';

        return $r;
    }
}

if (!function_exists('set_age')) {
    function set_age($naiss)
    {
        if ($naiss != null){
            $d = Carbon\Carbon::createFromFormat('Y-m-d',$naiss);
            return Carbon\Carbon::createFromDate($d->year, $d->month, $d->day)->age;
        }else{
            return null;
        }

    }
}



if (!function_exists('set_active')) {
    function set_active($active)
    {
        if ($active == 1)
        {
            $r= '<label class="label label-primary">Activé</label>';
        }else{
            $r= '<label class="label label-inverse">Désactivé</label>';
        }
        return $r;
    }
}

//if (!function_exists('create_notification')) {
//    function create_notification($libelle,$id)
//    {
//        \App\Models\Notification::create([
//            'destinataire'=>$id,
//            'libelle'=>$libelle,
//            'vu'=>false,
//            'type'=>1
//        ]);
//    }
//}

if (!function_exists('count_notification')) {
    function count_notification($id)
    {
        $r = DB::table('notifications')
            ->where('vu','=',0)
            ->where('destination','=',$id)
            ->count();
        return $r;
    }
}

if (!function_exists('count_notification_engager')) {
    function count_notification_engager($id)
    {
        $r = DB::table('notifications')
            ->where('vu','=',0)
            ->where('destination','=',$id)
            ->where('type','=',1)
            ->count();
        return $r;
    }
}

if (!function_exists('moy_note')) {
    function moy_note($id)
    {
        $r = DB::table('notes')
            ->where('id_personne','=',$id)
            ->avg('note_travailleur');
//        if ($r == null){$r = '---';}
        return $r;
    }
}

if (!function_exists('devis_exist')) {
    function devis_exist($id)
    {
        $r = DB::table('devis')
            ->where('id_requete','=',$id)
            ->first();
//        if ($r == null)
//        {
//            $r = '<i class="fa fa-times"></i>';
//        }else{
//            $r = '<i class="fa fa-file fa-check-square"></i>';
//        }
        return $r;
    }
}
