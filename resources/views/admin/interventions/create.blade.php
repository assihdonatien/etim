@include('layouts.headerAdmin')


    <div class="container">
        <div class="col-sm-12">
            <h3 class="text-center">Nouvelle intervention</h3>
        <br>
        <br>
        <br>

            <form class="col-sm-12" action="{{route('intervention.store')}}" method="post">
                {{csrf_field()}}

                <div class="col-sm-6">
                    <div class="form-group">
                        <label>Employé</label>
                        <select name="employe" id="" class="form-control">
                            <option value="">Sélectionnez un employé</option>
                            @foreach(\App\Employe::all()->sortBy('nomEmp') as $r)
                                <option value="{{$r->id}}">{{$r->nomEmp." ".$r->prenomEmp}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('employe'))
                            <p class="text-danger">{{ $errors->first('employe') }}</p>
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Activité ou problème</label>
                        <select name="probleme" id="" class="form-control">
                            <option value="">Faites une sélection</option>
                            @foreach(\App\Probleme::all()->sortBy('libelle') as $r)
                                <option value="{{$r->id}}">{{$r->libelle}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('probleme'))
                            <p class="text-danger">{{ $errors->first('probleme') }}</p>
                        @endif
                    </div>

                    <div class="form-group">
                        <label for="descriptionPro">Description du problème ou l'activité</label>
                        <textarea name="descriptionPro" id="" cols="30" rows="5" class="form-control"></textarea>
                        @if ($errors->has('descriptionPro'))
                            <p class="text-danger">{{ $errors->first('descriptionPro') }}</p>
                        @endif
                    </div>

                </div>
                <div class="col-sm-6">
                    <div class="col-sm">
                        <label>Description de l'action effectuée</label>
                        <textarea name="description" id="" cols="30" rows="5" class="form-control"></textarea>
                        @if ($errors->has('description'))
                            <p class="text-danger">{{ $errors->first('description') }}</p>
                        @endif
                    </div>

                    <div class="col-sm">
                        <label>Observations</label>
                        <select name="observation" id="" class="form-control">
                            <option value="">Sélectionnez une observation</option>
                            @foreach(\App\Observation::all() as $r)
                                <option value="{{$r->id}}">{{$r->libelleObservation}}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('observation'))
                            <p class="text-danger">{{ $errors->first('observation') }}</p>
                        @endif
                    </div>

                    <div class="col-sm">
                        <label for="debut">Date de début intervention</label>
                        <input type="datetime-local" name="debut" class="form-control">
                        @if ($errors->has('debut'))
                            <p class="text-danger">{{ $errors->first('debut') }}</p>
                        @endif
                    </div>


                    <div class="col-sm">
                        <label for="">Date de fin intervention</label>
                        <input type="datetime-local" name="fin" class="form-control">
                        @if ($errors->has('fin'))
                            <p class="text-danger">{{ $errors->first('fin') }}</p>
                        @endif
                    </div>
                </div>

                <HR />
                <button type="submit" class="col-sm-push btn btn-primary text-center">Valider</button>
            </form>

        </div>
    </div>

@include('layouts.footerAdmin')
