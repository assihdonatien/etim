@include('layouts.header1')

<div class="block-header">
    <h2> Panneau d'acceuil</h2>

    <ul class="actions">
        <li>
            <a href="">
                <i class="zmdi zmdi-trending-up"></i>
            </a>
        </li>
        <li>
            <a href="">
                <i class="zmdi zmdi-check-all"></i>
            </a>
        </li>
    </ul>

</div>


<div class="mini-charts">
    <div class="row">


        <div class="col-sm-6 col-md-3">
            <div class="mini-charts-item bgm-teal">
                <div class="clearfix">
                    <div class="chart stats-bar-2"></div>
                    <div class="count">
                        <small>
                            Alertes
                        </small>
                        <h2> {{count(\App\Alerte::all())}} </h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="mini-charts-item bgm-orange">
                <div class="clearfix">
                    <div class="chart stats-line"></div>
                    <div class="count">
                        <small>Problèmes</small>
                        <h2>
                            {{count(\App\Probleme::all())}}
                        </h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="mini-charts-item bgm-green">
                <div class="clearfix">
                    <div class="chart stats-line"></div>
                    <div class="count">
                        <small>Intervenants</small>
                        <h2>
                            {{count(\App\User::all())}}
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="row invoice text-center">
    <div class="col-lg-2 invoice text-center">
        <img class="" style="height: 150px; width: 190px" src="{{asset('MSC_NOIR.PNG')}}">
        {{--<br><h1>MEDITERRANEAN SHIPPING COMPANY</h1>--}}
    </div>
    <div class="col-lg-8 invoice text-center">
        <h1>MEDITERRANEAN SHIPPING COMPANY</h1>
    </div>
</div>



@include('layouts.footer1')
