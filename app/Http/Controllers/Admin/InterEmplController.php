<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InterEmplController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function index()
    {
        $user = Auth::user();
        return view('admin/etats/intervenant_par_employe/index',compact('user'));
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'intervenant'=>'required',
            'probleme'=>'required',
            'debut'=>'required',
            'fin'=>'required',
        ]);

        $da = DB::table('interventions')
            ->select(DB::raw('count(interventions.id) as inter'))
            ->join('users', 'users.id', '=', 'interventions.user_id')
            ->join('problemes', 'problemes.id', '=', 'interventions.probleme_id')
            ->where('user_id', '=', $request->intervenant)
            ->where('debut', '>=', $request->debut)
            ->where('fin', '<=', $request->fin)
            ->groupBy('employe_id')
            ->get();

        dd($da);
        return view('admin/etats/intervenant_par_employe/create',compact('user','da'));
    }
}
