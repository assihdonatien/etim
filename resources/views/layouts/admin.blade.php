<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->

<!-- Mirrored from 192.185.228.226/projects/ma/1-5-1/jquery/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 21 Oct 2015 02:43:46 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> IT-ISSUES </title>

    <!-- Vendor CSS -->
    <link href="{{asset('vendors/bower_components/fullcalendar/dist/fullcalendar.min.css')}} " rel="stylesheet">
    <link href="{{asset('vendors/bower_components/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css')}}" rel="stylesheet">



    <!-- CSS -->
    <link rel="icon" href="{{asset('img/default.jpg')}}">
    <link href="{{asset('css/app.min.1.css')}}" rel="stylesheet">
    <link href="{{asset('css/app.min.2.css')}}" rel="stylesheet">

</head>


<body>

@yield('content')

<footer id="footer">
    Copyright &copy; 2018 MSC - Togo

    <ul class="f-menu">
        <li><a href="">Acceuil</a></li>
        <li><a href="">Employés</a></li>
        <li><a href="">Problèmes</a></li>
        <li><a href="">Interventions</a></li>
        <li><a href="">Statistiques</a></li>
    </ul>
</footer>

<script src="{{asset('vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<script src="{{asset('vendors/bower_components/flot/jquery.flot.js')}}"></script>
<script src="{{asset('vendors/bower_components/flot/jquery.flot.resize.js')}}"></script>
<script src="{{asset('vendors/bower_components/flot.curvedlines/curvedLines.js')}}"></script>
<script src="{{asset('vendors/sparklines/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>

<script src="{{asset('vendors/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/fullcalendar/dist/fullcalendar.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/Waves/dist/waves.min.js')}}"></script>
<script src="{{asset('vendors/bootstrap-growl/bootstrap-growl.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js')}}"></script>


<script src="{{asset('vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js')}}"></script>

<script src="{{asset('js/flot-charts/curved-line-chart.js')}}"></script>
<script src="{{asset('js/flot-charts/line-chart.js')}}"></script>
<script src="{{asset('js/charts.js')}}"></script>

<script src="{{asset('js/charts.js')}}"></script>
<script src="{{asset('js/functions.js')}}"></script>
<script src="{{asset('js/demo.js')}}"></script>


</body>

</html>