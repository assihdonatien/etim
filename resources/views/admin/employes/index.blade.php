@include('layouts.headerAdmin')

<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <h2>Liste des employés <a href="{{route('employe.create')}}" class="btn btn-primary pull-right">
                    <i class="zmdi zmdi-plus"></i>
                    Nouvel Employé</a>
            </h2>
            <table class="table table-bordered table-hover tablesorter">
                <thead>
                <tr>
                    <th>Noms <i class="fa fa-sort"></i></th>
                    <th>Prénoms <i class="fa fa-sort"></i></th>
                    <th>Agences <i class="fa fa-sort"></i></th>
                    <th>Départements <i class="fa fa-sort"></i></th>
                </tr>
                </thead>
                <tbody>
                @foreach($employes->sortBy('nomEmp') as $r)
                    <tr>
                        <td>{{$r->nomEmp}}</td>
                        <td>{{$r->prenomEmp}}</td>
                        <td>{{$r->agence->libelleAgence}}</td>
                        <td>{{$r->departement->libelleDept}}</td>
                        <td>
                            <a href="{{route('employe.edit',$r->id)}}">
                                <button class="btn btn-warning" > <i class="zmdi zmdi-edit"></i></button>
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


@include('layouts.footerAdmin')