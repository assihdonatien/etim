<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterventions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interventions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('probleme_id')->unsigned();
            $table->foreign('probleme_id')->references('id')->on('problemes');

            $table->integer('employe_id')->unsigned();
            $table->foreign('employe_id')->references('id')->on('employes');

          $table->integer('observation_id')->unsigned();
//            $table->foreign('observation_id')->references('id')->on('observations');



            $table->string('description');
            $table->dateTime('debut');
            $table->dateTime('fin')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interventions');
    }
}
