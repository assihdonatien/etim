<?php

namespace App\Http\Controllers\Admin;

use App\Intervention;
//use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
//use Barryvdh\DomPDF\Facade as PDF;
use PDF;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class EtatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

//    public function print($data){
//        dd($data);
//    }

    public function index()
    {
        $user = Auth::user();
        return view('admin/etats/interventions_par_intervenants/index',compact('user'));
    }

    public function InterByEmpl()
    {
        $user = Auth::user();
        return view('admin/etats/interventions_par_employe/index',compact('user'));
    }

    public function InterByDept()
    {
        $user = Auth::user();
        return view('admin/etats/intervention_par_departement/index',compact('user'));
    }

    public function InterByProbleme()
    {
        $user = Auth::user();
        return view('admin/etats/interventions_par_problemes/index',compact('user'));
    }

    public function InterByInterByDept()
    {
        $user = Auth::user();
        return view('admin/etats/intervention_par_intervenant_pour_departement/index',compact('user'));
    }

    public function TotalEtat()
    {
        $user = Auth::user();
        return view('admin/etats/total_etats/index',compact('user'));
    }

    public function InterByProbByDept()
    {
        $user = Auth::user();
        return view('admin/etats/intervention_par_probleme_departement/index',compact('user'));
    }

    public function problemeRNR()
    {
        $user = Auth::user();
        return view('admin/etats/problemes_resolu_non_resolu/index',compact('user'));
    }

    public function doc22()
    {
        $user = Auth::user();
        return view('admin/etats/prnr/index',compact('user'));
    }


    public function create(Request $request)
    {
        $this->validate($request, [
            'intervenant'=>'required',
            'debut'=>'required',
            'fin'=>'required',
        ]);


        $data = DB::table('interventions')
            ->select(DB::raw('interventions.*,problemes.*, users.*,employes.*'))
            ->join('problemes','interventions.probleme_id','=','problemes.id')
            ->join('employes', 'interventions.employe_id','=','employes.id')
            ->join('users','interventions.user_id','=','users.id')
            ->where('users.id', '=', $request->intervenant)
            ->where('debut', '>=', $request->debut)
            ->where('fin', '<=', $request->fin)
            ->get();

        $nomIn = DB::table('users')
            ->select(DB::raw('users.*'))
            ->where('users.id', '=', $request->intervenant)
            ->get();


//        $prenomIn = $request->intervenat->prenom;
        $date_debut = \Carbon\Carbon::parse($request->debut)->format('d/m/y');
        $date_fin = \Carbon\Carbon::parse($request->fin)->format('d/m/y');

        $compte = count($data);

      //  return Excel::download(new UsersExport, 'users.xlsx');

       // PDF::loadView('admin/etats/interventions_par_intervenants/create',$data)->download('etat.pdf');
        return view('admin/etats/interventions_par_intervenants/doc',compact('user','data','compte','nomIn','date_fin','date_debut'));
    }

    public function interventionTotal(Request $request){
        $data = DB::table('interventions')
            ->select(DB::raw('count(problemes.id) as inter, problemes.*, users.*'))
            ->join('problemes','interventions.probleme_id','=','problemes.id')
            ->join('users','interventions.user_id','=','users.id')
            ->where('debut', '>=', $request->debut)
            ->where('fin', '<=', $request->fin)
            ->groupBy('problemes.id')
            ->get();

        $date_debut = \Carbon\Carbon::parse($request->debut)->format('d/m/y');
        $date_fin = \Carbon\Carbon::parse($request->fin)->format('d/m/y');
        return view('admin/etats/total_intervention_intervenant/doct',compact('user','data','date_debut','date_fin'));
    }

    public function interventionsByEmploye(Request $request){

//        $this->validate($request, [
////            'employe'=>'required',
//            'debut'=>'required',
//            'fin'=>'required',
//        ]);

        $data = DB::table('interventions')
            ->select(DB::raw('interventions.*,problemes.*, users.*,employes.*'))
            ->join('employes','interventions.employe_id','=','employes.id')
            ->join('problemes','interventions.probleme_id','=','problemes.id')
            ->join('users','interventions.user_id','=','users.id')
            ->where('employes.id', '=', $request->employe)
//            ->where('problemes.id','=', $request->probleme)
            ->where('debut', '>=', $request->debut)
            ->where('fin', '<=', $request->fin)
            //->having('problemes.libelle')
            ->get();

        $nomE = DB::table('employes')
            ->select(DB::raw('employes.*'))
            ->where('employes.id', '=', $request->employe)
            ->get();

       // dd($data);
        $comp = count($data);

        $date_debut = \Carbon\Carbon::parse($request->debut)->format('d/m/y');
        $date_fin = \Carbon\Carbon::parse($request->fin)->format('d/m/y');

       return view('admin/etats/interventions_par_employe/docInEmp', compact('user', 'data','nomE','comp','date_debut','date_fin'));

    }

    public function problemesResolus(Request $request){
        $data = DB::table('interventions')
            ->select(DB::raw('interventions.*, observations.*,problemes.*'))
            ->join('observations','interventions.observation_id','=','observations.id')
            ->join('problemes','interventions.probleme_id',"=",'problemes.id')
            ->where('debut', '>=', $request->debut)
            ->where('fin', '<=', $request->fin)
            ->get();


        $date_debut = \Carbon\Carbon::parse($request->debut)->format('d/m/y');
        $date_fin = \Carbon\Carbon::parse($request->fin)->format('d/m/y');

        return view('admin/etats/prnr/doc3', compact('user', 'data','date_debut','date_fin'));
    }

    public function interventionsByDepartement(Request $request){
        $data = DB::table('employes')
            ->select(DB::raw('interventions.*, problemes.*, departements.*'))
            ->join('departements','employes.departement_id','=','departements.id')
            ->join('interventions','interventions.employe_id','=','employes.id')
            ->join('problemes','interventions.probleme_id','=','problemes.id')
            ->where('debut', '>=', $request->debut)
            ->where('fin', '<=', $request->fin)
//            ->groupBy('departements.id')
            ->get();
        //dd($data);

        $date_debut = \Carbon\Carbon::parse($request->debut)->format('d/m/y');
        $date_fin = \Carbon\Carbon::parse($request->fin)->format('d/m/y');
       return view('admin/etats/intervention_par_departement/docInDept', compact('user', 'data','date_fin','date_debut'));
    }

    public function interventionsByProbleme(Request $request){
        $data = DB::table('interventions')
            ->select(DB::raw('count(interventions.id) as inter, problemes.*'))
            ->join('problemes','interventions.probleme_id','=','problemes.id')
            ->where('debut', '>=', $request->debut)
            ->where('fin', '<=', $request->fin)
            ->groupBy('problemes.id')
            ->get();
        //dd($data);
        $date_debut = \Carbon\Carbon::parse($request->debut)->format('d/m/y');
        $date_fin = \Carbon\Carbon::parse($request->fin)->format('d/m/y');
        return view('admin/etats/interventions_par_problemes/docInProbleme', compact('user', 'data','date_fin','date_debut'));
    }

    public function interventionsByIntervenantByDept(Request $request){

        $this->validate($request, [
           // 'intervenant'=>'required',
            'debut'=>'required',
            'fin'=>'required',
        ]);

        $data = DB::table('employes')
            ->select(DB::raw('count(interventions.id) as inter, departements.*, employes.* ,users.*'))
            ->join('departements','employes.departement_id','=','departements.id')
            ->join('interventions','interventions.employe_id','=','employes.id')
            ->join('users','interventions.user_id','=','users.id')
          //  ->where('users.id', '=', $request->intervenant)
            ->where('debut', '>=', $request->debut)
            ->where('fin', '<=', $request->fin)
            ->groupBy('departements.id')
            ->get();
       // dd($data);
        $date_debut = \Carbon\Carbon::parse($request->debut)->format('d/m/y');
        $date_fin = \Carbon\Carbon::parse($request->fin)->format('d/m/y');
        return view('admin/etats/intervention_par_intervenant_pour_departement/docDept', compact('user', 'data','date_fin','date_debut'));
    }

    public function totaux(Request $request){
        $interventions = DB::table('interventions')->select(DB::raw('count(interventions.id) as inter, interventions.description'))
           ->where('debut', '>=', $request->debut)
            ->where('fin', '<=', $request->fin);
        $employes = DB::table('employes')->select(DB::raw('count(employes.id) as inter, employes.nom'))
            ->where('debut', '>=', $request->debut)
           ->where('fin', '<=', $request->fin);
        $problemes = DB::table('problemes')->select(DB::raw('count(problemes.id) as inter, problemes.libelle'))
            ->where('debut', '>=', $request->debut)
           ->where('fin', '<=', $request->fin);
        $data = DB::table('users')->select(DB::raw('count(users.id) as inter, users.nom'))
            ->union($interventions)
            ->union($employes)
            ->union($problemes)
//            ->where('debut', '>=', $request->debut)
//            ->where('fin', '<=', $request->fin)
            ->get();

        $date_debut = \Carbon\Carbon::parse($request->debut)->format('d/m/y');
        $date_fin = \Carbon\Carbon::parse($request->fin)->format('d/m/y');
        //dd($data);
        return view('admin/etats/total_etats/docu', compact('user', 'data','date_debut','date_fin'));
    }

    public function interventionsByProblemeByDept(Request $request){
        $data = DB::table('employes')
            ->select(DB::raw('count(interventions.id) as inter, departements.*, problemes.*'))
            ->join('departements','employes.departement_id','=','departements.id')
            ->join('interventions','interventions.employe_id','=','employes.id')
            ->join('problemes','interventions.probleme_id','=','problemes.id')
            ->where('debut', '>=', $request->debut)
            ->where('fin', '<=', $request->fin)
            ->groupBy('departements.id')
            ->get();
        $date_debut = \Carbon\Carbon::parse($request->debut)->format('d/m/y');
        $date_fin = \Carbon\Carbon::parse($request->fin)->format('d/m/y');
        return view('admin/etats/intervention_par_probleme_departement/doc1', compact('user', 'data','date_debut','date_fin'));
        //dd($data);
    }

}
