<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::namespace('Admin')->group(function () {
    Route::get('/admin/profil','AdminController@index')->name('admin.profil');
    Route::get('/admin/etat/intervenants_par_interventions','EtatController@index')->name('admin.intervenants_par_interventions.index');
    Route::post('/admin/etat/intervenants_par_interventions','EtatController@create')->name('admin.intervenants_par_interventions.create');

    Route::get('/admin/etat/intervenants_par_employe','InterEmplController@index')->name('admin.intervenants_par_employe.index');
    Route::post('/admin/etat/intervenants_par_employe','InterEmplController@create')->name('admin.intervenants_par_employe.create');

    Route::get('/admin/etat/total_etats','TotalController@index')->name('admin.total_etats.index');
    Route::post('/admin/etat/total_etats','TotalController@create')->name('admin.total_etats.create');

    Route::get('/admin/etat','EtatController@create')->name('admin.etat.create');

    Route::get('/interventions/print/{data}',['as'=>'interventions.print','uses'=>'EtatController@print']);

    Route::resource('employe','EmployeController');

    Route::resource('intervenant','IntervenantController');

    Route::resource('intervention','InterventionController');

    Route::resource('probleme','ProblemeController');
});

