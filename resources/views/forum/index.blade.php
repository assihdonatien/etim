@include('layouts.headerAdmin')
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <h2>Liste du forum <a href="{{route('forum.create')}}" class="btn btn-primary pull-right"> Ajouter au forum</a></h2>
            <table class="table table-bordered table-hover tablesorter">
                <thead>
                <tr>
                    <th>Probleme <i class="fa fa-sort"></i></th>
                    <th>Contenu <i class="fa fa-sort"></i></th>
                    <th>Document joint<i class="fa fa-sort"></i></th>
                </tr>
                </thead>
                <tbody>
                @foreach($forum->sortBy('id_probleme') as $r)
                <tr>
                    <td>{{$r->id_probleme}}</td>
                    <td>{{$r->content}}</td>
                    <td>{{$r->document}}</td>
                    <td>
                        <a href="{{route('forum.edit',$r->id,$r->id_probleme)}}">
                            <button class="btn btn-warning"><i class="zmdi zmdi-edit"></i></button>
                        </a>
                    </td>

                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@include('layouts.footerAdmin')
