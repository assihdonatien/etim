@include('layouts.headerAdmin')

<div class="row">
    <div class="col-lg-12">
        <h3 class="text-center">Enrégister un nouvel employé</h3>

        <form class="col-lg-8 col-lg-offset-2 text-center" action="{{route('employe.store')}}" method="post">
                {{csrf_field()}}
            <div class="form-group">
                <label>Nom</label>
                <input class="form-control" type="text" name="nomEmp" required value="{{old('nomEmp')}}">
                @if ($errors->has('nomEmp'))
                    <p class="text-danger">{{ $errors->first('nomEmp') }}</p>
                @endif
            </div>

            <div class="form-group">
                <label>Prénom</label>
                <input class="form-control" type="text" name="prenomEmp" required value="{{old('prenomEmp')}}">
                @if ($errors->has('prenomEmp'))
                    <p class="text-danger">{{ $errors->first('prenomEmp') }}</p>
                @endif
            </div>

            <div class="form-group">
                <label>Agence</label>
                <select name="agence" id="" class="form-control">
                    <option value="">Sélectionnez une agence</option>
                    @foreach(\App\Agence::all()->sortBy('libelleAgence') as $a)
                        <option value="{{$a->id}}">{{$a->libelleAgence}}</option>
                    @endforeach
                </select>
                @if ($errors->has('agence'))
                    <p class="text-danger">{{ $errors->first('agence') }}</p>
                @endif
            </div>

            <div class="form-group">
                <label>Département</label>
                <select name="departement" id="" class="form-control">
                    <option value="">Sélectionnez un département</option>
                    @foreach(\App\Departement::all()->sortBy('libelleDept') as $d)
                        <option value="{{$d->id}}">{{$d->libelleDept}}</option>
                    @endforeach
                </select>
                @if ($errors->has('departement'))
                    <p class="text-danger">{{ $errors->first('departement') }}</p>
                @endif
            </div>


            <button type="submit" class="btn btn-primary">Valider</button>
        </form>

    </div>
</div>

@include('layouts.footerAdmin')
