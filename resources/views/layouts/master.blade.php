<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Dashboard - SB Admin</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('template/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="{{asset('template/css/sb-admin.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('template/font-awesome/css/font-awesome.min.css')}}">
    <!-- Page Specific CSS -->
    <link rel="stylesheet" href="http://cdn.oesmith.co.uk/morris-0.4.3.min.css">
</head>

<body>

<div id="wrapper">
    <!-- Sidebar -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.html">SB Admin</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <li class="active"><a href="/"><i class="fa fa-dashboard"></i> Accueil</a></li>
                <li><a href="{{route('intervenant.index')}}"><i class="fa fa-table"></i> Intervenants</a></li>
                <li><a href="{{route('intervention.index')}}"><i class="fa fa-table"></i> Interventions</a></li>
                <li><a href="{{route('employe.index')}}"><i class="fa fa-bar-chart-o"></i> Employés</a></li>
                <li><a href="{{route('probleme.index')}}"><i class="fa fa-bar-chart-o"></i> Problèmes</a></li>
                <li><a href="charts.html"><i class="fa fa-bar-chart-o"></i> Statistiques</a></li>

                <li><a href="forms.html"><i class="fa fa-edit"></i> Paramètres</a></li>
                {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Dropdown <b class="caret"></b></a>--}}
                    {{--<ul class="dropdown-menu">--}}
                        {{--<li><a href="#">Dropdown Item</a></li>--}}
                        {{--<li><a href="#">Another Item</a></li>--}}
                        {{--<li><a href="#">Third Item</a></li>--}}
                        {{--<li><a href="#">Last Item</a></li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
            </ul>

            <ul class="nav navbar-nav navbar-right navbar-user">
                @auth()
                    <li class="dropdown user-dropdown">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> {{$user->nom." ". $user->prenom}} <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{route('admin.profil')}}"><i class="fa fa-user"></i> Profile</a></li>
                            {{--<li><a href="#"><i class="fa fa-envelope"></i> Inbox <span class="badge">7</span></a></li>--}}
                            <li><a href="#"><i class="fa fa-gear"></i> Paramètres</a></li>
                            <li class="divider"></li>
                            <li>
                                <form action="{{route('logout')}}" method="post">
                                    {{csrf_field()}}
                                    <button type="submit" ><i class="fa fa-power-off"></i> Se Déconnecter</button>
                                </form>
                            </li>
                        </ul>
                    </li> 
                @endauth
                    @guest()
                        <li><a href="{{route('login')}}">Se Connecter</a> </li>
                    @endguest

            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">
        @if(session()->has('notification.message'))
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-{{session('notification.type')}} alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        {{session('notification.message')}}
                    </div>
                </div>
            </div>
        @endif
        @yield('content')

    </div><!-- /#page-wrapper -->

</div><!-- /#wrapper -->

<!-- JavaScript -->
<script src="{{asset('template/js/jquery-1.10.2.js')}}"></script>
<script src="{{asset('template/js/bootstrap.js')}}"></script>

<!-- Page Specific Plugins -->
<script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="http://cdn.oesmith.co.uk/morris-0.4.3.min.js"></script>
<script src="js/morris/chart-data-morris.js"></script>
<script src="js/tablesorter/jquery.tablesorter.js"></script>
<script src="js/tablesorter/tables.js"></script>

</body>
</html>
