<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Probleme extends Model
{
    protected $fillable = [
        'libelle','description','date_debut','date_fin'
    ];
}
