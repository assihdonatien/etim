
</div>

</section>

<br><br><br><br>

<footer id="footer">

    <ul class="f-menu">

        @if(Auth::user()->type==1)
        <li><a href="{{route('admin.profil')}}">Acceuil</a></li>
        <li><a href="{{route('alerte.index')}}">Alertes</a></li>
        <li><a href="{{route("employe.index")}}">Employés</a></li>
        <li><a href="{{route("intervenant.index")}}">Intervenants</a></li>
        <li><a href="{{route("probleme.index")}}">Problèmes</a></li>
        <li><a href="{{route("intervention.index")}}">Interventions</a></li>
        @endif


            @if(Auth::user()->type==0)
                <li><a href="{{route('admin.profil')}}">Acceuil</a></li>
                <li><a href="{{route('alerte.index')}}">Alertes</a></li>
            @endif
    </ul>

    Copyright &copy; 2018 MSC - Togo Developped by #asdoma
</footer>

<script src="{{asset('vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<script src="{{asset('vendors/bower_components/flot/jquery.flot.js')}}"></script>
<script src="{{asset('vendors/bower_components/flot/jquery.flot.resize.js')}}"></script>
<script src="{{asset('vendors/bower_components/flot.curvedlines/curvedLines.js')}}"></script>
<script src="{{asset('vendors/sparklines/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>

<script src="{{asset('vendors/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/fullcalendar/dist/fullcalendar.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/Waves/dist/waves.min.js')}}"></script>
<script src="{{asset('vendors/bootstrap-growl/bootstrap-growl.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js')}}"></script>

<script type="text/javascript" src="{{asset('js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/core/app.js')}}"></script>
<script type="text/javascript" src="{{asset('js/pages/datatables_sorting.js')}}"></script>

<script src="{{asset('//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js')}}"></script>


<script src="{{asset('vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js')}}"></script>

<script src="{{asset('js/flot-charts/curved-line-chart.js')}}"></script>
<script src="{{asset('js/flot-charts/line-chart.js')}}"></script>
<script src="{{asset('js/charts.js')}}"></script>
<script src="{{asset('toastr/toastr.min.js')}}"></script>

<script src="{{asset('js/charts.js')}}"></script>
<script src="{{asset('js/functions.js')}}"></script>
<script src="{{asset('js/demo.js')}}"></script>


<script>
    $(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ url('binder') }}',
            columns: [
                { data: 'nomEmp', name: 'nomEmp' },
                { data: 'prenomEmp', name: 'prenomEmp' },
                { data: 'prenom', name: 'prenom' },
                { data: 'nom', name: 'nom' }
            ]
        });
    });
</script>

@if(Auth::user()->type==1)
<script type="text/javascript">
    $(document).ready(function() {
        window.addEventListener('load', function () {
            Notification.requestPermission(function (status) {
                // Cela permet d'utiliser Notification.permission avec Chrome/Safari
                if (Notification.permission !== status) {
                    Notification.permission = status;
                }
            });
        });

        function alertChecker(){
            console.log('Checking alert system');
            $.get(
                '{{ url('alerte/alerting') }}',
                {criteria: $('meta[name="csrf-token"]').attr('content')},
                function (data) {
                    var dataParsed = JSON.parse(data);
                    //alert(dataParsed);
                    console.log('we are debugging');
                    console.log(data);
                    toastr.options = {
                        "closeButton": true,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "1000",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    $.each(dataParsed, function (index, d) {
                        // var toast = toastr.info('<p>'+d.description_alerte+'</p><b>'+d.date_alerte+'\',\'Alerte émise par: \'+d.nom+\' \'+d.prenom+\'<br>\'</b> <div><button type="button" class="btn btn-warning btn-sm transferer" id="'+d.id+'" onclick="alert(\'je suis transferer\')">Transférer</button><button type="button" class="btn btn-info btn-sm take" id="'+d.id+'" onclick="alert(\'Alerte je suis pris\');">Prendre</button></div>',''+d.titre_alerte+'',{
                        //     onclick:function () {
                        //         /*location.href="http://10.1.165.16/itissues/public"+"/alerte/intervenir?id="+d.id+"";*/
                        //         location.href="http://10.1.165.16/itissues/public"+"/alerte/intervenir?id="+d.id+"";
                        //     }
                        // });

                        var toast = toastr.info('<p>'+d.description_alerte+'</p><b>'+d.date_alerte+'</b> ','Alerte émise par: '+d.nom+' '+d.prenom+'<br>'+d.titre_alerte+'',{
                            onclick:function () {
                                location.href="http://10.1.165.117:8080/itissues/public"+"/alerte/intervenir?id="+d.id+"";
                            }
                        });
                        for (var i = 0; i < 10; i++) {
                        var ne = new Notification(d.titre_alerte + i,{
                            body:d.description_alerte,
                            tag:'soManyNotifications'
                        });}
                        //ne.show();
                        ne.onclick = function () {
                            parent.focus();
                            window.focus();
                            window.location.href="http://10.1.165.117:8080/itissues/public"+"/alerte/intervenir?id="+d.id+"";
                        };
                    });
                });
        }
        var timer  = setTimeout(function  myTimer(){
            // Si l'utilisateur accepte les notifications
            // essayons d'envoyer 10 notifications
            if (window.Notification && Notification.permission === "granted") {
                //function
                alertChecker();
            }
            // Si l'utilisateur n'a pas choisi s'il accepte d'être notifié // Note: à cause de Chrome, nous ne sommes pas certains que la
            // propriété permission soit définie, par conséquent il n'est pas
            // sûr de vérifier la valeur par défault.
            else if (window.Notification && Notification.permission !== "denied") {
                Notification.requestPermission(function (status) {
                    if (Notification.permission !== status) {
                        Notification.permission = status;
                    }

                    // Si l'utilisateur a accepté les notifications
                    if (status === "granted") {
                        //la meme fonction
                        alertChecker();
                    }

                    // Sinon on bascule sur une alerte modale
                    else {
                        //les notif classique a mettre ici
                        alert("Veuillez autoriser la notification pour ce site!");
                    }
                });
            }

            // Si l'utilisateur refuse les notifications
            else {
                //les notif classique a mettre ici
                alert("Veuillez autoriser la notification pour ce site!");
            }
            //alertChecker();
            timer = setTimeout(myTimer,60000);
        },1000)
        //$('.ckeditor').ckeditor();
    } );
</script>
@endif


</body>

</html>