@include('layouts.headerAdmin')

    <div class="row">
        <div class="col-lg-12">
            <h3 class="text-center">Ajouter au forum</h3>

            <form class="col-lg-8 col-lg-offset-2 text-center" action="{{route('forum.store')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <div class="form-group">
                    <label>Problème</label>
                    <select name="probleme" id="" class="form-control">
                        <option value="">Sélectionnez un problème</option>
                        @foreach(\App\Probleme::all()->sortBy('libelle') as $r)
                        <option value="{{$r->id}}">{{$r->libelle}}</option>
                        @endforeach
                    </select>
                    @if ($errors->has('probleme'))
                    <p class="text-danger">{{ $errors->first('probleme') }}</p>
                    @endif
                </div>

                <div class="form-group">
                    <label>Contenu</label>
                    <textarea class="form-control ckeditor" name="contenu" id="summary-ckeditor" cols="30" required rows="100">{{old('contenu')}}</textarea>
                    @if ($errors->has('contenu'))
                        <p class="text-danger">{{ $errors->first('contenu') }}</p>
                    @endif
                </div>

                <div class="form-group">
                    <label>Ajouter un fichier</label>
                    <input class="form-control" type="file" name="document[]" multiple required value="{{old('document')}}">
                    @if ($errors->has('document'))
                    <p class="text-danger">{{ $errors->first('document') }}</p>
                    @endif
                </div>
                <button type="submit" class="btn btn-primary">Valider</button>
            </form>
        </div>
    </div>
<scr
@include('layouts.footerAdmin')
