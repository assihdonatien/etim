{{--@include('layouts.headerAdmin')--}}
{{--<div class="row">--}}
    {{--<div class="col-lg-12">--}}
        {{--<div class="table-responsive">--}}
            {{--<h2>Liste des Alertes <a href="{{route('alerte.create')}}" class="btn btn-primary pull-right"> Nouvelle--}}
                    {{--alerte</a></h2>--}}
            {{--<table class="table table-bordered table-hover tablesorter">--}}
                {{--<thead>--}}
                {{--<tr>--}}
                    {{--<th>Titre <i class="fa fa-sort"></i></th>--}}
                    {{--<th>Description <i class="fa fa-sort"></i></th>--}}
                {{--</tr>--}}
                {{--</thead>--}}
                {{--<tbody>--}}
                {{--@foreach($alertes->sortBy('titre_alerte') as $r)--}}
                {{--<tr>--}}
                    {{--<td>{{$r->titre_alerte}}</td>--}}
                    {{--<td>{{$r->description_alerte}}</td>--}}
                    {{--<td>--}}
                        {{--<a href="{{route('alerte.edit',$r->idAlerte)}}">--}}
                            {{--<button class="btn btn-warning"><i class="zmdi zmdi-edit"></i></button>--}}
                        {{--</a>--}}
                    {{--</td>--}}

                {{--</tr>--}}
                {{--@endforeach--}}
                {{--</tbody>--}}
            {{--</table>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
{{--@include('layouts.footerAdmin')--}}


@include('layouts.headerAdmin')
<div class="row">
    <div class="col-lg-12">
        <div class="table-responsive">
            <h2>Liste des Alertes <a href="{{route('alerte.create')}}" class="btn btn-primary pull-right"> Nouvelle
                    alerte</a></h2>
            <table class="table table-bordered table-hover tablesorter">
                <thead>
                <tr>
                    <th>Date <i class="fa fa-sort"></i></th>
                    <th>Titre <i class="fa fa-sort"></i></th>
                    <th>Description <i class="fa fa-sort"></i></th>
                    <th>Etat <i class="fa fa-sort"></i></th>
                </tr>
                </thead>
                <tbody>

                @foreach($alertes->sortByDesc('date_alerte') as $r)

                    @if ($r->etat_alerte==0)
                        <tr class="clickable-row" data-href="http://10.1.165.16/itissues/public/alerte/intervenir?id={{$r->id}}">
                            <td><b>{{\Carbon\Carbon::parse($r->date_alerte)->format('d/m/Y H:i:s')}}</b></td>
                            <td>{{$r->titre_alerte}}</td>
                            <td>{{$r->description_alerte}}</td>
                            <td class="alert alert-danger">{{'En attente'}}</td>
                        </tr>
                    @endif

                    @if ($r->etat_alerte==1)
                        <tr>
                            <td><b>{{\Carbon\Carbon::parse($r->date_alerte)->format('d/m/Y H:i:s')}}</b></td>
                            <td>{{$r->titre_alerte}}</td>
                            <td>{{$r->description_alerte}}</td>
                            <td class="alert alert-success">{{'Traité'}}</td>
                        </tr>
                    @endif

                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@include('layouts.footerAdmin')
<script>
    $(".clickable-row").click(function () {
        window.location = $(this).data("href");
    });
</script>

