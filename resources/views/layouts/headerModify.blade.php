<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->

<!-- Mirrored from 192.185.228.226/projects/ma/1-5-1/jquery/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 21 Oct 2015 02:43:46 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> IT-ISSUES </title>

    <!-- Vendor CSS -->
    <link href="{{asset('vendors/bower_components/fullcalendar/dist/fullcalendar.min.css')}} " rel="stylesheet">
    <link href="{{asset('vendors/bower_components/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css')}}" rel="stylesheet">
    <link href="{{asset('vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css')}}" rel="stylesheet">



    <!-- CSS -->
    <link rel="icon" href="{{asset('img/default.jpg')}}">
    <link href="{{asset('css/app.min.1.css')}}" rel="stylesheet">
    <link href="{{asset('css/app.min.2.css')}}" rel="stylesheet">
    <link href="{{asset('//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css')}}" rel="stylesheet">
    <!-- DataTables Select CSS -->
    <link href="{{asset('css/addons/datatables-select.min.css')}}" rel="stylesheet">
    <!-- DataTables Select CSS -->
    <script href="{{asset('css/addons/datatables-select.min.js')}}" rel="stylesheet"></script>

</head>


<body>

<header id="header">
    <ul class="header-inner">
        <li id="menu-trigger" data-trigger="#sidebar">
            <div class="line-wrap">
                <div class="line top"></div>
                <div class="line center"></div>
                <div class="line bottom"></div>
            </div>
        </li>

        <li class="logo hidden-xs">
            <a href="{{route('admin.profil')}}"> IT-ISSUES </a>
        </li>

        <li class="pull-right">
        </li>
    </ul>

</header>

{

<section id="main">


    <section id="content">
        <div class="container">
