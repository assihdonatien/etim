@include('layouts.header1')

<div class="block-header">
    <h2> Panneau d'acceuil</h2>

    <ul class="actions">
        <li>
            <a href="">
                <i class="zmdi zmdi-trending-up"></i>
            </a>
        </li>
        <li>
            <a href="">
                <i class="zmdi zmdi-check-all"></i>
            </a>
        </li>
    </ul>

</div>


<div class="mini-charts">
    <div class="row">
        <div class="col-sm-6 col-md-3">
            <div class="mini-charts-item bgm-cyan">
                <div class="clearfix">
                    <div class="chart stats-bar"></div>
                    <div class="count">
                        <small>Nombre Total d'employés</small>
                        <h2>
                            {{count(\App\Employe::all())}}
                        </h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="mini-charts-item bgm-lightgreen">
                <div class="clearfix">
                    <div class="chart stats-bar-2"></div>
                    <div class="count">
                        <small>
                            Interventions
                        </small>
                        <h2> {{count(\App\Intervention::all())}} </h2>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-3">
            <div class="mini-charts-item bgm-orange">
                <div class="clearfix">
                    <div class="chart stats-line"></div>
                    <div class="count">
                        <small>Problèmes</small>
                        <h2>
                            {{count(\App\Probleme::all())}}
                        </h2>
                    </div>
                </div>
            </div>
        </div>

        
    </div>
</div>



<div class="row text-center">
    <div class="col-lg-2 invoice text-center" style="text-align-all: center">
        <img class="" style="height: 150px; width: 190px" src="{{asset('MSC_NOIR.PNG')}}">
        {{--<br><h1>MEDITERRANEAN SHIPPING COMPANY</h1>--}}
    </div>
    <div class="col-lg-8 invoice text-center" style="text-align-all: center">
        <h1>MEDITERRANEAN SHIPPING COMPANY</h1>
    </div>
</div>


<div class="dash-widgets">
    <div class="row">


    </div>
</div>

@include('layouts.footer1')