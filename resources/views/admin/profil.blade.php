@include('layouts.headerAdmin')

            <div class="block-header">
                <h2> Panneau d'acceuil</h2>

                <ul class="actions">
                    <li>
                        <a href="">
                            <i class="zmdi zmdi-trending-up"></i>
                        </a>
                    </li>
                    <li>
                        <a href="">
                            <i class="zmdi zmdi-check-all"></i>
                        </a>
                    </li>
                </ul>

            </div>


            <div class="mini-charts">
                <div class="row">
                    <div class="col-sm-6 col-md-3">
                        <div class="mini-charts-item bgm-cyan">
                            <div class="clearfix">
                                <div class="chart stats-bar"></div>
                                <div class="count">
                                    <small>Nombre Total d'employés</small>
                                    <h2>
                                        {{count(\App\Employe::all())}}
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="mini-charts-item bgm-lightgreen">
                            <div class="clearfix">
                                <div class="chart stats-bar-2"></div>
                                <div class="count">
                                    <small>
                                        Interventions
                                    </small>
                                    <h2> {{count(\App\Intervention::all())}} </h2>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="mini-charts-item bgm-orange">
                            <div class="clearfix">
                                <div class="chart stats-line"></div>
                                <div class="count">
                                    <small>Problèmes</small>
                                    <h2>
                                        {{count(\App\Probleme::all())}}
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-3">
                        <div class="mini-charts-item bgm-green">
                            <div class="clearfix">
                                <div class="chart stats-line"></div>
                                <div class="count">
                                    <small>Intervenants</small>
                                    <h2>
                                        {{count(\App\User::all())}}
                                    </h2>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <div class="center text-center">
                <div class="center">
                    <img class="" style="height: 150px; width: 190px" src="{{asset('MSC_NOIR.PNG')}}">
                    {{--<br><h1>MEDITERRANEAN SHIPPING COMPANY</h1>--}}
                </div>
                <div class="center">
                    <h1>MEDITERRANEAN SHIPPING COMPANY</h1>
                </div>
            </div>

@include('layouts.footerAdmin')