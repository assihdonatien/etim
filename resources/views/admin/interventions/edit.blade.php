@include('layouts.headerAdmin')


<div class="row">
    <div class="col-lg-12">
        <h3 class="text-center">Modification intervention</h3>

        <form class="col-lg-8 col-lg-offset-2 text-center" action="{{route('intervention.update', $intervention->id)}}" method="post">
            {{csrf_field()}}
            {{method_field('PUT')}}

            <div class="form-group">
                <label>Description du problème</label>
                <textarea name="descriptionPro" id="" cols="20" rows="3" class="form-control" >{{$intervention->descriptionPro}}</textarea>
                @if ($errors->has('descriptionPro'))
                    <p class="text-danger">{{ $errors->first('descriptionPro') }}</p>
                @endif
            </div>

            <div class="form-group">
                <label>Description de la résolution</label>
                <textarea name="description" id="" cols="30" rows="5" class="form-control" >{{$intervention->description}}</textarea>
                @if ($errors->has('description'))
                    <p class="text-danger">{{ $errors->first('description') }}</p>
                @endif
            </div>

            <div class="form-group">
                <label>Observations</label>
                <select name="observation" id="" class="form-control">
                    <option value="">Sélectionnez une observation</option>
                    @foreach(\App\Observation::all() as $r)
                        <option value="{{$r->id}}">{{$r->libelleObservation}}</option>
                    @endforeach
                </select>
                @if ($errors->has('observation'))
                    <p class="text-danger">{{ $errors->first('observation') }}</p>
                @endif
            </div>

            {{--<div class="form-group">--}}
                {{--<label for="">Date de début intervention</label>--}}
                {{--<input type="datetime-local" name="debut" class="form-control">--}}
                {{--@if ($errors->has('debut'))--}}
                    {{--<p class="text-danger">{{ $errors->first('debut') }}</p>--}}
                {{--@endif--}}
            {{--</div>--}}

            <a type="button" href="{{route('intervention.index')}}" class="btn btn-warning">Annuler</a>

            <button type="submit" class="btn btn-primary">Valider</button>

        </form>
    </div>
</div>

@include('layouts.footerAdmin')
