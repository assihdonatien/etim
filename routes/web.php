<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});

Route::post('/test1',['as'=>'etat1','uses'=>'Admin\EtatController@interventionsByEmploye']);
Route::get('/admin/etat/interventions_par_employe','Admin\EtatController@InterByEmpl')->name('admin.interventions_par_employe.index');

Route::post('/test2',['as'=>'etat2','uses'=>'Admin\EtatController@problemesResolus']);
Route::get('/admin/etat/prnr','Admin\EtatController@doc22')->name('admin.prnr.index');

Route::post('/test3',['as'=>'etat3','uses'=>'Admin\EtatController@interventionsByDepartement']);
Route::get('/admin/etat/intervention_par_departement','Admin\EtatController@InterByDept')->name('admin.intervention_par_departement.index');

Route::post('/test4',['as'=>'etat4','uses'=>'Admin\EtatController@interventionsByProbleme']);
Route::get('/admin/etat/interventions_par_problemes','Admin\EtatController@InterByProbleme')->name('admin.interventions_par_problemes.index');

Route::post('/test5',['as'=>'etat5','uses'=>'Admin\EtatController@interventionsByIntervenantByDept']);
Route::get('/admin/etat/intervention_par_intervenant_pour_departement','Admin\EtatController@InterByInterByDept')->name('admin.intervention_par_intervenant_pour_departement.index');

Route::post('/test6',['as'=>'etat6','uses'=>'Admin\EtatController@interventionsByProblemeByDept']);
Route::get('/admin/etat/intervention_par_probleme_departement','Admin\EtatController@InterByProbByDept')->name('admin.intervention_par_probleme_departement.index');

Route::post('/test7',['as'=>'etat7','uses'=>'Admin\EtatController@totaux']);
Route::get('/admin/etat/total_etats','Admin\EtatController@TotalEtat')->name('admin.total_etats.index');

Route::post('/test9',['as'=>'etat9','uses'=>'Admin\EtatController@interventionTotal']);
Route::get('/admin/etat/total_intervention_intervenant','Admin\EtatController@interventionTotal')->name('admin.total_intervention_intervenant.index');


Auth::routes();

//Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
//Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
//Route::post('password/reset', 'Auth\PasswordController@reset');


Route::get('/modify', 'HomeController@modify')->name('modify');
Route::post('/modifyValidation', 'HomeController@modifyValidation')->name('modifyValidation');


Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/intervention/binder', 'InterventionController@binder');


Route::namespace('Alerte')->group(function (){
   // Route::resource('alerte','Admin\AlerteController');
});


Route::namespace('Admin')->group(function () {

    Route::get('/alerte/alerting', 'AlerteController@alerting');
    Route::post('/alerte/alerting', 'AlerteController@alerting');

    Route::get('/alerte/sendemail', 'AlerteController@sendemail');
    Route::post('/alerte/sendemail', 'AlerteController@sendemail');

    Route::get('/alerte/intervenir', 'AlerteController@intervenir');
    Route::post('/alerte/intervenir', 'AlerteController@intervenir');

    Route::get('/intervenant/pass', 'AlerteController@intervenir');
    Route::post('/intervenant/pass', 'AlerteController@intervenir');

    //Route::get('/admin/modify', 'AdminController@modify')->name('modify');
    //Route::post('/admin/modifyValidation', 'AdminController@modifyValidation')->name('modifyValidation');

    Route::get('/admin/profil','AdminController@index')->name('admin.profil');

    Route::get('/admin/etat/intervenants_par_interventions','EtatController@index')->name('admin.intervenants_par_interventions.index');
    Route::post('/admin/etat/intervenants_par_interventions','EtatController@create')->name('admin.intervenants_par_interventions.create');

    Route::get('/admin/etat/total_etats','TotalController@index')->name('admin.total_etats.index');
    Route::post('/admin/etat/total_etats','TotalController@create')->name('admin.total_etats.create');

    Route::get('/admin/etat','EtatController@create')->name('admin.etat.create');

    Route::get('/interventions/print/{data}',['as'=>'interventions.print','uses'=>'EtatController@print']);

    Route::resource('employe','EmployeController');

    Route::resource('intervenant','IntervenantController');

    Route::resource('intervention','InterventionController');

    Route::resource('probleme','ProblemeController');

    Route::resource('alerte','AlerteController');

    Route::resource('forum','ForumController');
});


Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');