@include('layouts.headerAdmin')

    <div class="row">
        <div class="col-lg-12">
            <h3 class="text-center">Enrégister un nouveau problème</h3>

            <form class="col-lg-8 col-lg-offset-2 text-center" action="{{route('probleme.store')}}" method="post">
                {{csrf_field()}}
                <div class="form-group">
                    <label>Libelle</label>
                    <input class="form-control" type="text" name="libelle" required value="{{old('libelle')}}">
                    @if ($errors->has('libelle'))
                        <p class="text-danger">{{ $errors->first('libelle') }}</p>
                    @endif
                </div>

                {{--<div class="form-group">--}}
                    {{--<label>Description</label>--}}
                    {{--<textarea class="form-control" name="description" id="" cols="30" rows="5">{{old('description')}}</textarea>--}}
                    {{--@if ($errors->has('description'))--}}
                        {{--<p class="text-danger">{{ $errors->first('description') }}</p>--}}
                    {{--@endif--}}
                {{--</div>--}}

                <button type="submit" class="btn btn-primary">Valider</button>
            </form>
        </div>
    </div>
@include('layouts.footerAdmin')
