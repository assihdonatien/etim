@include('layouts.headerAdmin')

<div class="row">
    <div class="col-lg-12">
        <h3 class="text-center">Enrégister un nouvel intervenant</h3>

        <form class="col-lg-8 col-lg-offset-2 text-center" action="{{route('intervenant.update')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                <label>Nom</label>
                <input class="form-control" type="text" name="nom" required value="{{old('nom')}}" readonly="readonly">
                @if ($errors->has('nom'))
                    <p class="text-danger">{{ $errors->first('nom') }}</p>
                @endif
            </div>

            <div class="form-group">
                <label>Prénom</label>
                <input class="form-control" type="text" name="prenom" required value="{{old('prenom')}}" readonly="readonly">
                @if ($errors->has('prenom'))
                    <p class="text-danger">{{ $errors->first('prenom') }}</p>
                @endif
            </div>

            <div class="form-group">
                <label>Nom d'utilisateur</label>
                <input class="form-control" type="text" name="login" required value="{{old('login')}}">
                @if ($errors->has('login'))
                    <p class="text-danger">{{ $errors->first('login') }}</p>
                @endif
            </div>

            <div class="form-group">
                <label>Nouveau mot de passe</label>
                <input class="form-control" type="password" name="password" required>
                @if ($errors->has('password'))
                    <p class="text-danger">{{ $errors->first('password') }}</p>
                @endif
            </div>
            <div class="form-group">
                <label>Mot de passe (Confirmation)</label>
                <input class="form-control" type="password" name="password_confirmation" required>
                @if ($errors->has('password'))
                    <p class="text-danger">{{ $errors->first('password_confirmed') }}</p>
                @endif
            </div>

            <button type="submit" class="btn btn-primary">Valider</button>
        </form>

    </div>
</div>


@include('layouts.footerAdmin')
