<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employe extends Model
{
    protected $fillable = [
        'nom','prenom','departement'
    ];

    public function agence()
    {
        return $this->belongsTo('App\Agence','agence_id');
    }

    public function departement()
    {
        return $this->belongsTo('App\Departement','departement_id');
    }
}
