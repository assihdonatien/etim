<html>
<head>
    <meta charset="utf-8">
    <link href="{{asset('css/app.min.2.css')}} " rel="stylesheet">
    <link href="{{asset('template/css/bootstrap.css')}} " rel="stylesheet">
    <link href="{{asset('ressources/views/vendor/mail/html/themes/default.css')}} " rel="stylesheet">
    <link href="{{asset('_tables.scss')}} " rel="stylesheet">
</head>
<body>
<div>
    <div class="invoice center-block" id="printarea">
        <div class="row text-center">
            <div class="col-lg-2 invoice text-center" style="text-align-all: center">
                <img class="" style="height: 150px; width: 190px" src="{{asset('MSC_NOIR.PNG')}}">
                {{--<br><h1>MEDITERRANEAN SHIPPING COMPANY</h1>--}}
            </div>
            <div class="col-lg-8 invoice text-center" style="text-align-all: center">
                <h1>MEDITERRANEAN SHIPPING COMPANY</h1>
            </div>
        </div>
        <hr style="font-weight: bold">
        <div class="row">
            <h2 style="text-align: center;">RAPPORT DU : {{$date_debut}} AU {{$date_fin}}</h2>
            {{--<h3 style="text-align: center;">Nombre d'interventions par employe</h3>--}}
            <h3 style="text-align: center;">Nombre d'interventions pour l'employé     @foreach($nomE as $n)
                    <tr>
                        <td>{{$n->nomEmp." ".$n->prenomEmp}}</td>
                    </tr>
                @endforeach</h3>
            <div class="col-sm-12">
                <table class="table table-striped table-hover table-responsive table-bordered">
                    <thead>
                    <tr>
                        <th>Nom & Prénoms de l'intervenant</th>
                        <th>Libellé du problème</th>
                        <th>Description du problème</th>
                        <th>Date de début</th>
                        <th>Durée</th>
                        <th>Durée</th>
                        <th>Date de fin</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($data->sortByDesc('fin') as $r)
                        <tr>
                            <td >{{$r->nom." ".$r->prenom}}</td>
                            <td >{{$r->libelle}}</td>
                            <td >{{$r->descriptionPro}}</td>
                            <td >{{$r->debut}}</td>
                            <td >{{$r->duree}}</td>
                            <td >{{$r->duration1}}</td>
                            <td >{{$r->fin}}</td>
                        </tr>
                    @endforeach
                    </tbody>

                    <tfoot><h4>Nombre total d'interventions: {{$comp}}</h4></tfoot>

                </table>
            </div>
        </div>
        <div class="panel panel-footer">
            <h5>Edité le <?= date('d/m/Y à H:i:s')?>, par {{Auth::user()->nom}} {{Auth::user()->prenom}} </h5>
        </div>
        <div class="center text-center">
            <a type="button" href="{{route('intervention.index')}}" class="btn btn-warning">Annuler</a>
            <button type="button" value="Print" id="btnPrint" onclick="print()" class="btn btn-primary hidden-print" style="">Télécharger</button>
        </div>


    </div>
</div>
<script type="text/javascript">
    $('#btnPrint').live("click", function () {
        var divContents = $('#printarea').html();
        var WinPrint = window.open('','','height = 400, width = 800');
        // WinPrint.document.write("<link rel='stylesheet' href='style.css' type='text/css' media='print' />");
        WinPrint.document.write('<html><head><title>Contenu DIV</title>');
        WinPrint.document.write(divContents);
        WinPrint.document.write('</body></html>');
        WinPrint.document.close();
        WinPrint.focus();
        WinPrint.print();
    });

    // WinPrint.close();
    // }
</script>
</body>
</html>