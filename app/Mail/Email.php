<?php

namespace App\Mail;

use App\Alerte;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Email extends Mailable
{
    use Queueable, SerializesModels;

    public $tos = array();
    public $sender;
    public $alerte;

    /**
     * Create a new message instance.
     *
     * @return void
     */



    public function __construct(Alerte $alerte)
    {
        $this->alerte = $alerte;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $sender = User::where('id', $this->alerte->employe_id_alerte)->firstOrFail();
        return $this->from($sender->email, $sender->nom)->subject($sender->nom . " " . $sender->prenom . " : ITISSUES")->view('alerte.email');
    }
}
