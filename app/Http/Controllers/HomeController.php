<?php

namespace App\Http\Controllers;

use App\Intervention;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function modifyValidation(){
        if(isset($_POST['password'],$_POST['password_confirm'])){
            $password = \Illuminate\Support\Facades\Input::post('password');
            $confirm = \Illuminate\Support\Facades\Input::post('password_confirm');
            $login = \Illuminate\Support\Facades\Input::post('login');
    
            if ($password == $confirm){
                $user = User::where('login', $login)->firstOrFail();
                $user->password = bcrypt($confirm);
                $user->first_connection = 1;
                $user->save();
    
                if (Auth::guard('admin')->attempt(['login' => $login, 'password' => $password, 'type'=>1]))
                {
                    return redirect(route('admin.profil'));
                }
    
                if (Auth::guard('admin')->attempt(['login' => $login, 'password' => $password, 'type'=>0]))
                {
                    return redirect(route('home'));
                }
            }else{
                flash('Les deux mots de passe ne sont pas conformes', 'danger');
                return redirect('profil');
            }
        }
    }
    
    public function modify(){
        if (isset($_GET['user'])){
            $user = $_GET['user'];
            $useri = User::where('id', $user)->firstOrFail();
    
            return view('admin.modify', compact('useri'));
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
}
