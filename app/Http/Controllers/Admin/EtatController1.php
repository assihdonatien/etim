<?php

namespace App\Http\Controllers\Admin;

use App\Intervention;
//use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
//use Barryvdh\DomPDF\Facade as PDF;
use PDF;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class EtatController1 extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function print($data)
    {
        dd($data);
    }

    public function index()
    {
        $user = Auth::user();
        return view('admin/etats/interventions_par_intervenants/index', compact('user'));
    }


    public function create(Request $request)
    {
        $this->validate($request, [
            'intervenant' => 'required',
            'debut' => 'required',
            'fin' => 'required',
        ]);


        $data = DB::table('interventions')
            ->select(DB::raw('count(problemes.id) as inter, problemes.*, users.*'))
            ->join('problemes', 'interventions.probleme_id', '=', 'problemes.id')
            ->join('users', 'interventions.user_id', '=', 'users.id')
            ->where('users.id', '=', $request->intervenant)
            ->where('debut', '>=', $request->debut)
            ->where('fin', '<=', $request->fin)
            ->groupBy('problemes.id')
            ->get();


//        $pdf = App::make('dompdf.wrapper');
//        $pdf = PDF::loadView('admin/etats/interventions_par_intervenants/doc', compact('data'));
//        $pdf->stream('etat.pdf');
//        //return $pdf->download('etat.pdf');
        //  return Excel::download(new UsersExport, 'users.xlsx');

        // PDF::loadView('admin/etats/interventions_par_intervenants/create',$data)->download('etat.pdf');
        return view('admin/etats/interventions_par_intervenants/doc', compact('user', 'data'));
    }

//    public function total(Request $request){
//        $this->validate($request, [
//            'intervenant'=>'required',
//            'intervention'=>'required',
//            'employe'=>'required',
//            'probleme'=>'required',
//            'departement'=>'required',
//            'debut'=>'required',
//            'fin'=>'required',
//        ]);
//
//        $employe = DB::table('employes')
//            ->select(DB::raw('count(employes.id) as empl'))
//            ->where('departements.id','=',$request->departement)
//            ->where('debut', '>=', $request->debut)
//            ->where('fin', '<=', $request->fin)
//            ->get();
//
//       return view('admin/etats/total_etats/create',compact('user','employe'));
//
////        $pdf = App::make('dompdf.wrapper');
////        $pdf = PDF::loadView('admin/etats/interventions_par_intervenants/doc', compact('employe'));
////        $pdf->stream('etat.pdf');
//    }
}
