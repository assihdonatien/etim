@include('layouts.headerAdmin')

<div class="row">
    <div class="col-lg-3">
        <img class="img-responsive " style="height: 250px; width: 250px" src="{{asset('MSC_NOIR.PNG')}}">
    </div>
    <div class="col-lg-6">
        <br><br><br><br><br>
        <h3>MEDITERRANEAN SHIPPING COMPANY</h3>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <h3 class="text-center">Nombre d'interventions par intervenant</h3>
        <div class="table-responsive">
            <table class="table table-bordered table-hover tablesorter">
                <thead>
                <tr>
                    <th>Noms & Prénoms<i class="fa fa-sort"></i></th>
                    <th>Problèmes <i class="fa fa-sort"></i></th>
                    <th>Total par problèmes <i class="fa fa-sort"></i></th>
                </tr>
                </thead>
                <tbody>
                @foreach($data as $r)
                    <tr>
                        <td>{{$r->nom." ".$r->prenom}}</td>
                        <td>{{$r->libelle}}</td>
                        <td>{{$r->inter}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <a class="btn btn-primary"> <i class="fa fa-print">Télécharger</i></a>
        </div>
    </div>
</div>

@include('layouts.footerAdmin')
