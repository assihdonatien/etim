@include('layouts.headerAdmin')

<div class="row">
    <div class="col-lg-12">
        <h3 class="text-center">Interventions par problème</h3>

        <form class="col-lg-8 col-lg-offset-2 text-center" action="{{route('etat4')}}" method="post">
            {{csrf_field()}}


            <div class="form-group">
                <label for="">Du</label>
                <input type="date" name="debut" class="form-control">
                @if ($errors->has('fin'))
                    <p class="text-danger">{{ $errors->first('debut') }}</p>
                @endif
            </div>

            <div class="form-group">
                <label for="">Au</label>
                <input type="date" name="fin" class="form-control">
                @if ($errors->has('fin'))
                    <p class="text-danger">{{ $errors->first('fin') }}</p>
                @endif
            </div>

            <button type="submit" class="btn btn-primary">Voir état</button>
        </form>
    </div>
</div>

@include('layouts.footerAdmin')
