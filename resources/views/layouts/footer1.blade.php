
</div>

</section>

<br><br><br><br>

<footer id="footer">
    Copyright &copy; 2018 MSC - Togo Developped by #asdoma

    <ul class="f-menu">
        <li><a href="{{route('admin.profil')}}">Acceuil</a></li>
        <li><a href="{{route("alerte.index")}}">Alertes</a></li>

    </ul>




</footer>

<script src="{{asset('vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<script src="{{asset('vendors/bower_components/flot/jquery.flot.js')}}"></script>
<script src="{{asset('vendors/bower_components/flot/jquery.flot.resize.js')}}"></script>
<script src="{{asset('vendors/bower_components/flot.curvedlines/curvedLines.js')}}"></script>
<script src="{{asset('vendors/sparklines/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js')}}"></script>

<script src="{{asset('vendors/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/fullcalendar/dist/fullcalendar.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/jquery.nicescroll/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/Waves/dist/waves.min.js')}}"></script>
<script src="{{asset('vendors/bootstrap-growl/bootstrap-growl.min.js')}}"></script>
<script src="{{asset('vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js')}}"></script>


<script src="{{asset('vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js')}}"></script>

<script src="{{asset('js/flot-charts/curved-line-chart.js')}}"></script>
<script src="{{asset('js/flot-charts/line-chart.js')}}"></script>
<script src="{{asset('js/charts.js')}}"></script>

<script src="{{asset('js/charts.js')}}"></script>
<script src="{{asset('js/functions.js')}}"></script>
<script src="{{asset('js/demo.js')}}"></script>


</body>

</html>