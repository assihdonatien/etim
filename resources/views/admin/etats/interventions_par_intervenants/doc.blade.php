<html>
<head>
    <meta charset="utf-8">
    <link href="{{asset('css/app.min.2.css')}} " rel="stylesheet">
    <link href="{{asset('template/css/bootstrap.css')}} " rel="stylesheet">
    <link href="{{asset('ressources/views/vendor/mail/html/themes/default.css')}} " rel="stylesheet">
    <link href="{{asset('_tables.scss')}} " rel="stylesheet">
</head>
<body>
<div>
    <div class="invoice center-block" id="printarea">
        <div class="row text-center">
            <div class="invoice text-center" style="text-align-all: center">
                <img class="" style="height: 150px; width: 190px" src="{{asset('MSC_NOIR.PNG')}}">
                {{--<br><h1>MEDITERRANEAN SHIPPING COMPANY</h1>--}}
            </div>
            <div class="invoice text-center" style="text-align-all: center">
                <h1>MEDITERRANEAN SHIPPING COMPANY</h1>
            </div>
        </div>
        <hr style="font-weight: bold">
        <div class="row">
            <h2 style="text-align: center;">RAPPORT DU : {{$date_debut}} AU {{$date_fin}}</h2>
            <h3 style="text-align: center;">Nombre d'interventions pour l'intervenant     @foreach($nomIn as $n)
                    <tr>
                        <td>{{$n->nom." ".$n->prenom}}</td>
                    </tr>
                @endforeach</h3>
            <div class="col-sm-12">
                <table class="table table-striped table-hover table-responsive table-bordered">
                    <thead>
                    <tr>
                        <th>Nom de l'employé</th>
                        <th>Problemes</th>
                        <th>Description du problème</th>
                        <th>Description de la résolution</th>
                        <th>Date de début intervention</th>
                        <th>Durée d'interventions</th>
                        <th>Durée d'interventions</th>
                        <th>Date de fin intervention</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($data->sortByDesc('fin') as $dat)
                        <tr>
                            <td >{{$dat->nomEmp." ".$dat->prenomEmp}}</td>
                            <td >{{$dat->libelle}}</td>
                            <td >{{$dat->descriptionPro}}</td>
                            <td >{{$dat->description}}</td>
                            <td >{{\Carbon\Carbon::parse($dat->debut)->format('d/m/Y H:i:s')}}</td>
                            <td >{{$dat->duree}}</td>
                            <td >{{$dat->duration1}}</td>
                            <td >{{\Carbon\Carbon::parse($dat->fin)->format('d/m/Y H:i:s')}}</td>
                        </tr>
                    @endforeach
                    </tbody>

                    <tfoot><h4>Nombre total d'interventions: {{$compte}}</h4></tfoot>
                </table>
            </div>
        </div>
        <div class="panel panel-footer">
            <h5>Edité le <?= date('d/m/Y à H:i:s')?>, par {{Auth::user()->nom}} {{Auth::user()->prenom}} </h5>
        </div>
        <button type="button" href="{{route('admin.intervenants_par_interventions.index')}}" class="btn btn-warning">Annuler</button>
        <button type="button" value="Print" id="btnPrint" onclick="print()" class="btn btn-primary hidden-print">Télécharger</button>
    </div>
</div>
<script type="text/javascript">
    $('#btnPrint').live("click", function () {
        var divContents = $('#printarea').html();
        var WinPrint = window.open('','','height = 375, width = 775');
        // WinPrint.document.write("<link rel='stylesheet' href='style.css' type='text/css' media='print' />");
        WinPrint.document.write('<html><head><title>Contenu DIV</title>');
        WinPrint.document.write(divContents);
        WinPrint.document.write('</body></html>');
        WinPrint.document.close();
        WinPrint.focus();
        WinPrint.print();
    });

    // WinPrint.close();
    // }
</script>
</body>
</html>